<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BiroController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MagangController;
use App\Http\Controllers\PetugasBiroController;
use App\Http\Controllers\SekolahController;
use App\Http\Controllers\SiswaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', [AuthController::class, 'login']);
// Route::post('/', [AuthController::class, 'loginPost']);

Route::controller(AuthController::class)->group(function () {
    Route::get('/login', 'login')->name('login')->middleware('guest');
    Route::post('/login', 'loginPost')->name('loginPost')->middleware('guest');
    Route::get('/logout', 'logout')->name('logout')->middleware('auth');
});

Route::controller(DashboardController::class)->group(function () {
    Route::get('/', 'index')->name('dashboard')->middleware('auth');
});

Route::controller(SekolahController::class)->group(function () {
    Route::get('/sekolah', 'sekolah')->name('sekolah')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::post('/sekolah', 'sekolahPost')->name('sekolahPost')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::post('/sekolah/put', 'sekolahPut')->name('sekolahPut')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::get('/sekolah/{npsn}/delete', 'sekolahDelete')->name('sekolahDelete')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::get('sekolah/export/pdf', 'exportPdf')->name('sekolahExportPdf')->middleware(['auth', 'role:Admin,Petugas Biro']);

    Route::get('/sekolah/{npsn}/penanggung-jawab', 'penanggungJawab')->name('daftarPenanggungJawabSekolah')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::get('/sekolah/penanggung-jawab', 'penanggungJawab')->name('penanggungJawabSekolah')->middleware(['auth', 'role:Sekolah']);
    Route::post('/sekolah/penanggung-jawab', 'penanggungJawabPost')->name('penanggungJawabSekolahPost')->middleware(['auth', 'role:Sekolah']);
    Route::get('/sekolah/penanggung-jawab/{nik}/delete', 'penanggungJawabDelete')->name('penanggungJawabSekolahDelete')->middleware(['auth', 'role:Sekolah']);
});

Route::controller(BiroController::class)->group(function () {
    Route::get('/biro', 'biro')->name('biro')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::post('/biro', 'biroPost')->name('biroPost')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::post('/biro/put', 'biroPut')->name('biroPut')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::get('/biro/{id}/delete', 'biroDelete')->name('biroDelete')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::get('biro/export/pdf', 'exportPdf')->name('biroExportPdf')->middleware(['auth', 'role:Admin,Petugas Biro']);

    Route::get('/biro/{id}/penanggung-jawab', 'penanggungJawab')->name('penanggungJawabBiro')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::post('/biro/penanggung-jawab', 'penanggungJawabPost')->name('penanggungJawabBiroPost')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::get('/biro/{id}/penanggung-jawab/{nik}/delete', 'penanggungJawabDelete')->name('penanggungJawabBiroDelete')->middleware(['auth', 'role:Admin,Petugas Biro']);

    Route::get('/biro/siswa-magang', 'siswaMagang')->name('siswaMagang')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::get('/biro/siswa-magang/export/pdf', 'siswaMagangExportPdf')->name('siswaMagangExportPdf')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::post('/biro/siswa-magang/penilaian', 'penilaianSiswaMagang')->name('penilaianSiswaMagang')->middleware(['auth', 'role:Admin,Petugas Biro']);
});

Route::controller(PetugasBiroController::class)->group(function () {
    Route::get('/petugas-biro', 'petugasBiro')->name('petugasBiro')->middleware(['auth', 'role:Admin']);
    Route::post('/petugas-biro', 'petugasBiroPost')->name('petugasBiroPost')->middleware(['auth', 'role:Admin']);
    Route::get('/petugas-biro/{nik}/delete', 'petugasBiroDelete')->name('petugasBiroDelete')->middleware(['auth', 'role:Admin']);
});

Route::controller(SiswaController::class)->group(function () {
    Route::get('/siswa', 'siswa')->name('siswa')->middleware(['auth', 'role:Sekolah']);
    Route::post('/siswa', 'siswaPost')->name('siswaPost')->middleware(['auth', 'role:Sekolah']);
    Route::get('/siswa/{nik}/delete', 'siswaDelete')->name('siswaDelete')->middleware(['auth', 'role:Sekolah']);
});

Route::controller(MagangController::class)->group(function () {
    Route::get('/magang', 'magang')->name('magang')->middleware(['auth', 'role:Sekolah,Petugas Biro,Admin']);
    Route::post('/magang/pengajuan', 'pengajuanMagang')->name('pengajuanMagang')->middleware(['auth', 'role:Sekolah']);
    Route::get('/magang/pengajuan/{id}/delete', 'pengajuanMagangDelete')->name('pengajuanMagangDelete')->middleware(['auth', 'role:Sekolah']);
    Route::get('/magang/pengajuan/{id}/verif', 'verifMagang')->name('verifMagang')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::post('/magang/pengajuan/tolak', 'tolakMagang')->name('tolakMagang')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::get('/magang/pengajuan/export/pdf', 'pengajuanExportPdf')->name('pengajuanExportPdf')->middleware(['auth', 'role:Admin,Petugas Biro']);

    Route::get('/magang/siswa/distribusi', 'distribusiSiswa')->name('distribusiSiswa')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::post('/magang/siswa/distribusi', 'distribusiSiswaPost')->name('distribusiSiswaPost')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::get('/magang/siswa/distribusi/{nisn}/delete', 'distribusiSiswaDelete')->name('distribusiSiswaDelete')->middleware(['auth', 'role:Admin,Petugas Biro']);

    Route::get('/magang/absensi', 'absensi')->name('absensi')->middleware(['auth']);
    Route::post('/magang/absensi', 'absensiPost')->name('absensiPost')->middleware(['auth', 'role:Siswa']);
    Route::get('/magang/absensi/{id}/batal', 'batalAbsen')->name('batalAbsen')->middleware(['auth', 'role:Siswa']);
    Route::get('/magang/absensi/export/pdf', 'absensiExportPdf')->name('absensiExportPdf')->middleware(['auth', 'role:Admin,Petugas Biro']);
    Route::post('/magang/absensi/batas', 'batasAbsenPut')->name('batasAbsenPut')->middleware(['auth', 'role:Admin,Petugas Biro']);

    Route::get('/magang/jurnal-kegiatan', 'jurnalKegiatan')->name('jurnalKegiatan')->middleware(['auth']);
    Route::post('/magang/jurnal-kegiatan', 'jurnalKegiatanPost')->name('jurnalKegiatanPost')->middleware(['auth', 'role:Siswa']);
    Route::get('/magang/jurnal-kegiatan/{id}/delete', 'jurnalKegiatanDelete')->name('jurnalKegiatanDelete')->middleware(['auth', 'role:Siswa']);
    Route::get('/magang/jurnal/export/pdf', 'jurnalExportPdf')->name('jurnalExportPdf')->middleware(['auth', 'role:Admin,Petugas Biro']);

    Route::get('/magang/laporan-akhir', 'laporanAkhir')->name('laporanAkhir')->middleware(['auth']);
    Route::post('/magang/laporan-akhir', 'laporanAkhirPost')->name('laporanAkhirPost')->middleware(['auth', 'role:Siswa']);
    Route::get('/magang/laporan-akhir/delete', 'laporanAkhirDelete')->name('laporanAkhirDelete')->middleware(['auth', 'role:Siswa']);
    Route::get('/magang/laporan-akhir/export/pdf', 'laporanAkhirExportPdf')->name('laporanAkhirExportPdf')->middleware(['auth', 'role:Admin,Petugas Biro']);
});
