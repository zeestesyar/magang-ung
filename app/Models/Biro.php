<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Biro extends Model
{
    use HasFactory, HasUuids;
    protected $table = 'biro';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = ['nama_biro'];

    public function siswaMagang() {
        return $this->hasMany(SiswaMagang::class, 'id_biro');
    }

    public function penanggungJawabBiro() {
        return $this->hasMany(PenanggungJawabBiro::class, 'id_biro');
    }
}
