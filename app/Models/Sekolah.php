<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sekolah extends Model
{
    use HasFactory;
    protected $table = 'sekolah';
    protected $primaryKey = 'npsn';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = ['npsn', 'nama_sekolah', 'id_user'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function periodeMagang() {
        return $this->hasMany(PeriodeMagang::class, 'npsn');
    }

    public function siswa() {
        return $this->hasMany(Siswa::class, 'npsn');
    }

    public function penanggungJawabSekolah() {
        return $this->hasMany(PenanggungJawabSekolah::class, 'npsn');
    }
}
