<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;
    protected $table = 'siswa';
    protected $primaryKey = 'nisn';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = ['nisn', 'nik', 'npsn'];

    public function biodata() {
        return $this->belongsTo(Biodata::class, 'nik');
    }

    public function sekolah() {
        return $this->belongsTo(Sekolah::class);
    }

    public function siswaMagang() {
        return $this->hasOne(SiswaMagang::class, 'nisn');
    }
}
