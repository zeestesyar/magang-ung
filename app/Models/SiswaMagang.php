<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiswaMagang extends Model
{
    use HasFactory;
    protected $table = 'siswa_magang';
    protected $primaryKey = 'nisn';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = ['nisn', 'id_periode', 'url_laporan', 'penilaian'];

    public function siswa() {
        return $this->belongsTo(Siswa::class, 'nisn');
    }
    public function periodeMagang() {
        return $this->belongsTo(PeriodeMagang::class, 'id_periode');
    }
    public function biro() {
        return $this->belongsTo(Biro::class, 'id_biro');
    }

    public function kehadiranSiswa() {
        return $this->hasMany(KehadiranSiswa::class, 'nisn');
    }
}
