<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenanggungJawabSekolah extends Model
{
    use HasFactory;
    protected $table = 'penanggung_jawab_sekolah';
    protected $primaryKey = 'nik';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = ['nik', 'npsn'];

    public function biodata() {
        return $this->belongsTo(Biodata::class, 'nik');
    }
}
