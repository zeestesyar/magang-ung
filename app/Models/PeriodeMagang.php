<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PeriodeMagang extends Model
{
    use HasFactory, HasUuids;
    protected $table = 'periode_magang';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = ['tmt', 'tst', 'npsn', 'url_surat', 'sts_persetujuan', 'alasan'];

    public function sekolah() {
        return $this->belongsTo(Sekolah::class, 'npsn');
    }

    public function siswaMagang() {
        return $this->hasMany(SiswaMagang::class, 'id_periode');
    }

    // public function siswa() {
    //     return $this->hasManyThrough(Siswa::class, SiswaMagang::class, 'id_periode', 'nisn', 'id', 'nisn');
    // }
}
