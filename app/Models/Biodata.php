<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    use HasFactory;
    protected $table = 'biodata';
    protected $primaryKey = 'nik';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = ['nik', 'nama_lengkap', 'tanggal_lahir', 'tempat_lahir', 'alamat', 'jenis_kelamin', 'id_user', 'no_telp'];

    public function user() {
        return $this->belongsTo(User::class, 'nik');
    }

    public function siswa() {
        return $this->hasOne(Siswa::class, 'nik');
    }
}
