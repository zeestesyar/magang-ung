<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KehadiranSiswa extends Model
{
    use HasFactory, HasUuids;
    protected $table = 'kehadiran_siswa';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'uuid';
    protected $fillable = ['nisn', 'tanggal', 'sts_hadir', 'url_foto', 'alasan'];

    public function siswaMagang()
    {
        return $this->belongsTo(SiswaMagang::class, 'nisn');
    }

    public function kegiatanSiswa() {
        return $this->hasMany(KegiatanSiswa::class, 'id_kehadiran');
    }
}
