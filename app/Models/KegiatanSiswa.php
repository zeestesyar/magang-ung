<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KegiatanSiswa extends Model
{
    use HasFactory, HasUuids;
    protected $table = 'kegiatan_siswa';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = ['url_foto', 'deskripsi', 'id_kehadiran'];

    public function kehadiranSiswa()
    {
        return $this->belongsTo(KehadiranSiswa::class, 'id_kehadiran');
    }
}
