<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BatasAbsensi extends Model
{
    use HasFactory, HasUuids;
    protected $table = 'batas_absensi';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'uuid';
    public $timestamps = false;
    protected $fillable = ['batas'];
}
