<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenanggungJawabBiro extends Model
{
    use HasFactory;
    protected $table = 'penanggung_jawab_biro';
    protected $primaryKey = 'nik';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = ['nik', 'id_biro'];

    public function biro() {
        return $this->belongsTo(Biro::class, 'id_biro');
    }

    public function biodata() {
        return $this->belongsTo(Biodata::class, 'nik');
    }
}
