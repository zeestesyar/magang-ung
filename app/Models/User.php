<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, HasUuids;
    protected $table = 'user';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'uuid';
    protected $fillable = ['username', 'password', 'role'];

    public function biodata() {
        return $this->hasOne(Biodata::class, 'id_user');
    }

    public function sekolah() {
        return $this->hasOne(Sekolah::class, 'id_user');
    }
}
