<?php

namespace App\Http\Controllers;

use App\Models\Biodata;
use App\Models\Biro;
use App\Models\KegiatanSiswa;
use App\Models\PeriodeMagang;
use App\Models\Sekolah;
use App\Models\Siswa;
use App\Models\SiswaMagang;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    function index(Request $request)
    {
        $user = $request->user();
        if (in_array($user->role, ['Admin', 'Petugas Biro'])) {
            $data['siswa'] = SiswaMagang::whereNotNull('id_biro')->count();
            $data['sekolah'] = Sekolah::count();
            $data['biro'] = Biro::count();
            $periodeMagang = PeriodeMagang::get();
            $data['pengajuan'] = $periodeMagang->count();
            $data['pengajuanDiterima'] = $periodeMagang->filter(function($item, $key) {
                return $item->sts_persetujuan;
            })->count();
            $data['pengajuanDitolak'] = $periodeMagang->filter(function($item, $key) {
                return !$item->sts_persetujuan && !is_null($item->sts_persetujuan);
            })->count();
        } else if ($user->role == 'Sekolah') {
            $siswaMagang = SiswaMagang::whereHas('periodeMagang', function ($query) use ($user) {
                $query->where('npsn', '=', $user->sekolah->npsn);
            })->whereNotNull('id_biro')->get();
            $data['jurnalHarian'] = KegiatanSiswa::whereHas('kehadiranSiswa', function ($query) use ($user) {
                $query->whereHas('siswaMagang', function ($query) use ($user) {
                    $query->whereHas('periodeMagang', function ($query) use ($user) {
                        $query->where('npsn', $user->sekolah->npsn);
                    });
                });
            })->count();
            $data['siswa'] = $siswaMagang->count();
            $data['laporanAkhir'] = $siswaMagang->filter(function ($item) {
                return $item->url_laporan != null;
            })->count();
            $periodeMagang = PeriodeMagang::where('npsn', $user->sekolah->npsn)->get();
            $data['pengajuan'] = $periodeMagang->count();
            $data['pengajuanDiterima'] = $periodeMagang->filter(function($item, $key) {
                return $item->sts_persetujuan;
            })->count();
            $data['pengajuanDitolak'] = $periodeMagang->filter(function($item, $key) {
                return !$item->sts_persetujuan;
            })->count();
        } else if ($user->role == 'Siswa') {
            $data['jurnalHarian'] = KegiatanSiswa::whereHas('kehadiranSiswa', function ($query) use($user) {
                $query->where('nisn', Siswa::select('nisn')->where('nik', '=', $user->biodata->nik)->first()->nisn);
            })->count();
        }
        return view('index', $data);
    }
}
