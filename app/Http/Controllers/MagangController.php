<?php

namespace App\Http\Controllers;

use App\Models\BatasAbsensi;
use App\Models\Biro;
use App\Models\KegiatanSiswa;
use App\Models\KehadiranSiswa;
use App\Models\PeriodeMagang;
use App\Models\Sekolah;
use App\Models\Siswa;
use App\Models\SiswaMagang;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MagangController extends Controller
{
    function magang(Request $request)
    {
        $user = $request->user();
        if ($user->role == 'Sekolah') {
            $npsn = $user->sekolah->npsn;
            $periodeMagang = PeriodeMagang::with('siswaMagang.siswa.biodata')->where('npsn', '=', $npsn)->get();
            $siswa = Siswa::with('biodata')->where('npsn', '=', $npsn)->doesntHave('siswaMagang')->get();
            $compact = compact('periodeMagang', 'siswa');
        } else {
            $query = Sekolah::with(['periodeMagang.siswaMagang.siswa.biodata']);
            $sekolah = $query->get();
            if (is_null($request['sekolah'])) {
                $table = $sekolah;
            } else {
                $table = $query->where('npsn', $request['sekolah'])->get();
            }

            $compact = compact('sekolah', 'table');
        }
        return view('magang', $compact);
    }

    function pengajuanExportPdf(Request $request)
    {
        if (is_null($request['sekolah'])) {
            $sekolah = Sekolah::with(['periodeMagang.siswaMagang.siswa.biodata'])->get();
        } else {
            $sekolah = Sekolah::with(['periodeMagang.siswaMagang.siswa.biodata'])->where('npsn', $request['sekolah'])->get();
        }
        return Pdf::loadView('pdf.pengajuan', compact('sekolah'))->download('Data Pengajuan (' . Carbon::now() . ').pdf');
    }

    function pengajuanMagang(Request $request)
    {
        $validated = $request->validate([
            'jangka-waktu' => 'required',
            'file' => 'required|file|mimes:pdf',
            'nisn' => 'required'
        ]);
        $waktu = explode(' - ', $validated['jangka-waktu']);
        $path = $request->file('file')->storeAs('uploads/pdf', Str::uuid() . '.pdf');
        $periodeMagang = PeriodeMagang::create([
            'tmt' => Carbon::parse($waktu[0])->format('Y-m-d'),
            'tst' => Carbon::parse($waktu[1])->format('Y-m-d'),
            'npsn' => $request->user()->sekolah->npsn,
            'url_surat' => $path
        ]);
        foreach ($validated['nisn'] as $nisn) {
            SiswaMagang::create([
                'nisn' => $nisn,
                'id_periode' => $periodeMagang->id,
            ]);
        }
        return redirect(route('magang'));
    }

    function verifMagang($id)
    {
        PeriodeMagang::find($id)->update([
            'sts_persetujuan' => true
        ]);
        return redirect(route('magang'));
    }

    function tolakMagang(Request $request)
    {
        PeriodeMagang::find($request['id-pengajuan'])->update([
            'sts_persetujuan' => false,
            'alasan' => str_replace(["\r", "\n"], ["\\r", "\\n"], $request['alasan'])
        ]);
        return redirect(route('magang'));
    }

    function pengajuanMagangDelete($id)
    {
        SiswaMagang::where('id_periode', '=', $id)->delete();
        PeriodeMagang::find($id)->delete();
        return redirect(route('magang'));
    }

    function distribusiSiswa()
    {
        $data['biro'] = Biro::with(['siswaMagang.siswa.biodata', 'siswaMagang.periodeMagang.sekolah'])->get();
        $idPeriode = PeriodeMagang::select('id')->where('sts_persetujuan', '=', true)->get();
        $data['siswa'] = SiswaMagang::with('siswa.biodata')->whereNull('id_biro')->whereIn('id_periode', $idPeriode)->get();
        return view('distribusi-siswa', $data);
    }

    function distribusiSiswaPost(Request $request)
    {
        $validated = $request->validate([
            'id-biro' => 'required',
            'nisn' => 'required'
        ]);

        foreach ($validated['nisn'] as $nisn) {
            SiswaMagang::where('nisn', '=', $nisn)->update(['id_biro' => $validated['id-biro']]);
        }
        return redirect(route('distribusiSiswa'));
    }

    function distribusiSiswaDelete($nisn)
    {
        SiswaMagang::where('nisn', '=', $nisn)->update(['id_biro' => null]);
        return redirect(route('distribusiSiswa'));
    }

    function absensi(Request $request)
    {
        $user = $request->user();
        if ($user->role == 'Siswa') {
            $batasAbsensi = BatasAbsensi::first();
            if(is_null($batasAbsensi)) {

                $batasAbsen = Carbon::parse('12:00:00')->toTimeString();
            } else {
                $batasAbsen = Carbon::parse($batasAbsensi->batas)->toTimeString();
            }
            $now = Carbon::now()->toTimeString();
            $kehadiranSiswa = [];
            $bisaAbsen = false;
            $siswa = Siswa::whereHas('siswaMagang', function ($query) {
                $query->whereHas('periodeMagang', function ($query) {
                    $now = Carbon::now();
                    $query->where('sts_persetujuan', true)->where('tmt', '<=', $now)->where('tst', '>=', $now);
                })->whereNotNull('id_biro');
            })->where('nik', $user->biodata->nik)->get();
            if ($siswa->isNotEmpty()) {
                $bisaAbsen = true;
                $kehadiranSiswa = KehadiranSiswa::withExists('kegiatanSiswa')->where('nisn', $siswa->first()->nisn)->orderByDesc('tanggal')->get();
                $bisaAbsen = $now <= $batasAbsen;
                if ($kehadiranSiswa->isNotEmpty() && $bisaAbsen) {
                    $bisaAbsen = Carbon::today()->gt(Carbon::parse($kehadiranSiswa->first()->tanggal));
                }

            }

            $compact = compact('kehadiranSiswa', 'bisaAbsen');
        } else if ($user->role == 'Sekolah') {
            $query = PeriodeMagang::with(['siswaMagang.kehadiranSiswa', 'siswaMagang.siswa.biodata'])->where('npsn', $user->sekolah->npsn);
            $periodeMagang = $query->get();
            if (is_null($request['id-periode'])) {
                $table = $periodeMagang;
            } else if (is_null($request['siswa'])) {
                $table = $query->where('id', $request['id-periode'])->get();
            } else {
                $table = $query->with([
                    'siswaMagang' => function ($query) use ($request) {
                        $query->where('nisn', $request['siswa']);
                    }
                ])->where('id', $request['id-periode'])->get();
            }
            $compact = compact('periodeMagang', 'table');
        } else {
            $batasAbsensi = BatasAbsensi::first();
            if(is_null($batasAbsensi)) {
                $batasAbsen = '12:00:00';
            } else {
                $batasAbsen = $batasAbsensi->batas;
            }
            $query = Sekolah::with([
                'periodeMagang.siswaMagang.kehadiranSiswa',
                'periodeMagang.siswaMagang.siswa.biodata',
                'periodeMagang' => function ($query) {
                    $query->where('sts_persetujuan', true);
                }
            ]);
            $sekolah = $query->get();
            if (is_null($request['sekolah'])) {
                $table = $sekolah;
            } else if (is_null($request['id-periode'])) {
                $table = $query->where('npsn', $request['sekolah'])->get();
            } else if (is_null($request['siswa'])) {
                $table = $query->with([
                    'periodeMagang' => function ($query) use ($request) {
                        $query->where('id', $request['id-periode']);
                    }
                ])->get();
            } else {
                $table = $query->with([
                    'periodeMagang.siswaMagang' => function ($query) use ($request) {
                        $query->where('nisn', $request['siswa']);
                    }
                ])->get();
            }
            $compact = compact('sekolah', 'table', 'batasAbsen');
        }

        return view('absensi', $compact);
    }

    function absensiExportPdf(Request $request)
    {
        if (is_null($request['sekolah'])) {
            $sekolah = Sekolah::with([
                'periodeMagang.siswaMagang.kehadiranSiswa',
                'periodeMagang.siswaMagang.siswa.biodata',
                'periodeMagang' => function ($query) {
                    $query->where('sts_persetujuan', true);
                }
            ])->get();
        } else if (is_null($request['id-periode'])) {
            $sekolah = Sekolah::with([
                'periodeMagang.siswaMagang.kehadiranSiswa',
                'periodeMagang.siswaMagang.siswa.biodata',
                'periodeMagang' => function ($query) {
                    $query->where('sts_persetujuan', true);
                }
            ])->where('npsn', $request['sekolah'])->get();
        } else if (is_null($request['siswa'])) {
            $sekolah = Sekolah::with([
                'periodeMagang.siswaMagang.kehadiranSiswa',
                'periodeMagang.siswaMagang.siswa.biodata',
                'periodeMagang' => function ($query) use ($request) {
                    $query->where('sts_persetujuan', true)->where('id', $request['id-periode']);
                }
            ])->get();
        } else {
            $sekolah = Sekolah::with([
                'periodeMagang.siswaMagang.kehadiranSiswa',
                'periodeMagang.siswaMagang.siswa.biodata',
                'periodeMagang' => function ($query) use ($request) {
                    $query->where('sts_persetujuan', true)->where('id', $request['id-periode']);
                },
                'periodeMagang.siswaMagang' => function ($query) use ($request) {
                    $query->where('nisn', $request['siswa']);
                }
            ])->get();
        }
        // return view('pdf.absensi', compact('sekolah'));
        return Pdf::loadView('pdf.absensi', compact('sekolah'))->download('Data Absensi (' . Carbon::now() . ').pdf');
    }

    function absensiPost(Request $request)
    {
        $validated = $request->validate([
            'file' => 'required|file|mimes:jpg',
            'sts-hadir' => 'required|in:Hadir,Izin,Sakit',
            'alasan' => 'required_unless:sts-hadir,Hadir'
        ]);

        $path = $request->file('file')->storeAs('uploads/image', Str::uuid() . '.jpg');

        if ($validated['sts-hadir'] == 'Hadir') {
            KehadiranSiswa::create([
                'nisn' => Siswa::where('nik', $request->user()->biodata->nik)->first()->nisn,
                'tanggal' => Carbon::now()->toDateString(),
                'sts_hadir' => $validated['sts-hadir'],
                'url_foto' => $path
            ]);
        } else {
            KehadiranSiswa::create([
                'nisn' => Siswa::where('nik', $request->user()->biodata->nik)->first()->nisn,
                'tanggal' => Carbon::now()->toDateString(),
                'sts_hadir' => $validated['sts-hadir'],
                'url_foto' => $path,
                'alasan' => str_replace(["\r", "\n"], ["\\r", "\\n"], $validated['alasan'])
            ]);
        }

        return redirect(route('absensi'));
    }

    function batalAbsen($id)
    {
        KehadiranSiswa::find($id)->delete();
        return redirect(route('absensi'));
    }

    function batasAbsenPut(Request $request) {
        $validated = $request->validate([
            'batas-absensi' => 'required'
        ]);

        $batas = BatasAbsensi::first();
        if(is_null($batas)) {
            BatasAbsensi::create([
                'batas' => $validated['batas-absensi']
            ]);
        } else {
            $batas->update([
                'batas' => $validated['batas-absensi']
            ]);
        }
        return redirect(route('absensi'));
    }

    function jurnalKegiatan(Request $request)
    {
        $user = $request->user();
        if ($user->role == 'Siswa') {
            $nisn = Siswa::select('nisn')->where('nik', '=', $request->user()->biodata->nik)->first()->nisn;
            $kehadiranSiswa = KehadiranSiswa::with('kegiatanSiswa')->where('nisn', $nisn)->where('sts_hadir', 'Hadir')->orderByDesc('tanggal')->get();
            $hadir = false;
            if ($kehadiranSiswa->isNotEmpty()) {
                $hadir = $kehadiranSiswa->first()->tanggal == Carbon::today()->toDateString();
            }
            $compact = compact('kehadiranSiswa', 'hadir');
        } else if ($user->role == 'Sekolah') {
            $query = PeriodeMagang::with(['siswaMagang.kehadiranSiswa.kegiatanSiswa', 'siswaMagang.siswa.biodata'])->where('npsn', $user->sekolah->npsn);
            $periodeMagang = $query->get();
            if (is_null($request['id-periode'])) {
                $table = $periodeMagang;
            } else if (is_null($request['siswa'])) {
                $table = $query->where('id', $request['id-periode'])->get();
            } else {
                $table = $query->with([
                    'siswaMagang' => function ($query) use ($request) {
                        $query->where('nisn', $request['siswa']);
                    }
                ])->where('id', $request['id-periode'])->get();
            }
            $compact = compact('periodeMagang', 'table');
        } else {
            $query = Sekolah::with(['periodeMagang.siswaMagang.kehadiranSiswa.kegiatanSiswa', 'periodeMagang.siswaMagang.siswa.biodata']);
            $sekolah = $query->get();
            if (is_null($request['sekolah'])) {
                $table = $sekolah;
            } else if (is_null($request['id-periode'])) {
                $table = $query->where('npsn', $request['sekolah'])->get();
            } else if (is_null($request['siswa'])) {
                $table = $query->with([
                    'periodeMagang' => function ($query) use ($request) {
                        $query->where('id', $request['id-periode']);
                    }
                ])->get();
            } else {
                $table = $query->with([
                    'periodeMagang.siswaMagang' => function ($query) use ($request) {
                        $query->where('nisn', $request['siswa']);
                    }
                ])->get();
            }
            $compact = compact('sekolah', 'table');
        }
        return view('jurnal-kegiatan', $compact);
    }

    function jurnalExportPdf(Request $request)
    {

        if (is_null($request['sekolah'])) {
            $sekolah = Sekolah::with(['periodeMagang.siswaMagang.kehadiranSiswa.kegiatanSiswa', 'periodeMagang.siswaMagang.siswa.biodata'])->get();
        } else if (is_null($request['id-periode'])) {
            $sekolah = Sekolah::with(['periodeMagang.siswaMagang.kehadiranSiswa.kegiatanSiswa', 'periodeMagang.siswaMagang.siswa.biodata'])->where('npsn', $request['sekolah'])->get();
        } else if (is_null($request['siswa'])) {
            $sekolah = Sekolah::with([
                'periodeMagang.siswaMagang.kehadiranSiswa.kegiatanSiswa',
                'periodeMagang.siswaMagang.siswa.biodata',
                'periodeMagang' => function ($query) use ($request) {
                    $query->where('id', $request['id-periode']);
                }
            ])->where('npsn', $request['sekolah'])->get();
        } else {
            $sekolah = Sekolah::with([
                'periodeMagang.siswaMagang.kehadiranSiswa.kegiatanSiswa',
                'periodeMagang.siswaMagang.siswa.biodata',
                'periodeMagang.siswaMagang' => function ($query) use ($request) {
                    $query->where('nisn', $request['siswa']);
                }
            ])->get();
        }
        // return view('pdf.jurnal', compact('sekolah'));
        return Pdf::loadView('pdf.jurnal', compact('sekolah'))->download('Data Jurnal (' . Carbon::now() . ').pdf');
    }

    function jurnalKegiatanPost(Request $request)
    {
        $validated = $request->validate([
            'file' => 'required|file|mimes:jpg',
            'deskripsi' => 'required'
        ]);
        $path = $request->file('file')->storeAs('uploads/image', Str::uuid() . '.jpg');
        KegiatanSiswa::create([
            'nisn' => Siswa::select('nisn')->where('nik', '=', $request->user()->biodata->nik)->first()->nisn,
            'url_foto' => $path,
            'deskripsi' => str_replace(["\r", "\n"], ["\\r", "\\n"], $validated['deskripsi']),
            'id_kehadiran' => $request['id-kehadiran']
        ]);

        return redirect(route('jurnalKegiatan'));
    }

    function jurnalKegiatanDelete($id)
    {
        KegiatanSiswa::find($id)->delete();
        return redirect(route('jurnalKegiatan'));
    }

    function laporanAkhir(Request $request)
    {
        $user = $request->user();
        if ($user->role == 'Siswa') {
            $biodata = $user->biodata;
            $data['laporan'] = SiswaMagang::find(Siswa::where('nik', '=', $biodata->nik)->first()->nisn);
        } else if ($user->role == 'Sekolah') {
            $query = PeriodeMagang::with([
                'siswaMagang.siswa.biodata',
                'siswaMagang' => function ($query) use ($user) {
                    $query->whereNotNull('url_laporan');
                }
            ])->where('npsn', '=', $user->sekolah->npsn);
            $periodeMagang = $query->get();
            if (is_null($request['id-periode'])) {
                $table = $periodeMagang;
            } else {
                $table = $query->where('id', $request['id-periode'])->get();
            }
            $data = compact('periodeMagang', 'table');
        } else {
            $query = Sekolah::with([
                'periodeMagang.siswaMagang' => function ($query) {
                    $query->whereNotNull('url_laporan');
                },
                'periodeMagang.siswaMagang.siswa.biodata'
            ]);
            $sekolah = $query->get();
            if (is_null($request['sekolah'])) {
                $table = $sekolah;
            } else if (is_null($request['id-periode'])) {
                $table = $query->where('npsn', $request['sekolah'])->get();
            } else {
                $table = $query->with([
                    'periodeMagang' => function ($query) use ($request) {
                        $query->where('id', $request['id-periode']);
                    }
                ])->get();
            }

            $data = compact('sekolah', 'table');
        }
        return view('laporan-akhir', $data);
    }

    function laporanAkhirExportPdf(Request $request)
    {
        if (is_null($request['sekolah'])) {
            $sekolah = Sekolah::with([
                'periodeMagang.siswaMagang' => function ($query) {
                    $query->whereNotNull('url_laporan');
                },
                'periodeMagang.siswaMagang.siswa.biodata'
            ])->get();
        } else if (is_null($request['id-periode'])) {
            $sekolah = Sekolah::with([
                'periodeMagang.siswaMagang' => function ($query) {
                    $query->whereNotNull('url_laporan');
                },
                'periodeMagang.siswaMagang.siswa.biodata'
            ])->where('npsn', $request['sekolah'])->get();
            // $table = $query;
        } else {
            $sekolah = Sekolah::with([
                'periodeMagang.siswaMagang' => function ($query) {
                    $query->whereNotNull('url_laporan');
                },
                'periodeMagang' => function ($query) use ($request) {
                    $query->where('id', $request['id-periode']);
                },
                'periodeMagang.siswaMagang.siswa.biodata'
            ])->get();
        }
        // return view('pdf.laporan-akhir', compact('sekolah'));
        return Pdf::loadView('pdf.laporan-akhir', compact('sekolah'))->download('Data Laporan Akhir (' . Carbon::now() . ').pdf');
    }

    function laporanAkhirPost(Request $request)
    {
        $request->validate([
            'file' => 'required|file|mimes:pdf'
        ]);
        $biodata = $request->user()->biodata;
        $path = $request->file('file')->storeAs('uploads/pdf', Str::uuid() . '.pdf');
        $siswaMagang = SiswaMagang::find(Siswa::where('nik', '=', $biodata->nik)->first()->nisn);
        $siswaMagang->update(['url_laporan' => $path]);
        return redirect(route('laporanAkhir'));
    }

    function laporanAkhirDelete(Request $request)
    {
        $biodata = $request->user()->biodata;
        $siswaMagang = SiswaMagang::find(Siswa::where('nik', '=', $biodata->nik)->first()->nisn);
        $siswaMagang->update(['url_laporan' => null]);
        return redirect(route('laporanAkhir'));
    }
}
