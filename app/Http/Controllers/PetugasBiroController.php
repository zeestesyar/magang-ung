<?php

namespace App\Http\Controllers;

use App\Models\Biodata;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PetugasBiroController extends Controller
{
    function petugasBiro() {
        $data['petugasBiro'] = User::where('role', '=', 'Petugas Biro')->with('biodata')->get()->map(function ($user) {
            return $user->biodata;
        });
        return view('petugas-biro', $data);
    }

    function petugasBiroPost(Request $request) {
        $validated = $request->validate([
            'nik' => 'required|numeric|digits:16|unique:biodata,nik',
            'nama-lengkap' => 'required',
            'tempat-lahir' => 'required',
            'tanggal-lahir' => 'required|date_format:Y-m-d',
            'alamat' => 'required',
            'jenis-kelamin' => 'required|in:Laki - laki,Perempuan',
            'username' => 'required|unique:user,username',
            'password' => 'required'
        ]);

        $user = User::create([
            'username' => $validated['username'],
            'password' => bcrypt($validated['password']),
            'role' => 'Petugas Biro'
        ]);

        Biodata::create([
            'nik' => $validated['nik'],
            'nama_lengkap' => $validated['nama-lengkap'],
            'tanggal_lahir' => $validated['tanggal-lahir'],
            'tempat_lahir' => $validated['tempat-lahir'],
            'alamat' => $validated['alamat'],
            'jenis_kelamin' => $validated['jenis-kelamin'],
            'id_user' => $user->id
        ]);

        return redirect(route('petugasBiro'));
    }

    function petugasBiroDelete($nik) {
        $biodata = Biodata::find($nik);
        $id_user = $biodata->id_user;
        $biodata->delete();
        User::find($id_user)->delete();

        return redirect(route('petugasBiro'));
    }
}
