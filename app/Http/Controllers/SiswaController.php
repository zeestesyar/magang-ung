<?php

namespace App\Http\Controllers;

use App\Models\Biodata;
use App\Models\Siswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiswaController extends Controller
{
    function siswa(Request $request)
    {
        $npsn = $request->user()->sekolah->npsn;
        $data['siswa'] = Siswa::withExists('siswaMagang')->with('biodata')->where('npsn', '=', $npsn)->get();
        return view('siswa', $data);
    }

    function siswaPost(Request $request)
    {
        $validated = $request->validate([
            'nik' => 'required|numeric|digits:16|unique:biodata,nik',
            'nisn' => 'required|numeric|digits:10|unique:siswa,nisn',
            'nama-lengkap' => 'required',
            'tempat-lahir' => 'required',
            'tanggal-lahir' => 'required|date_format:Y-m-d',
            'alamat' => 'required',
            'jenis-kelamin' => 'required|in:Laki - laki,Perempuan',
            // 'username' => 'required|unique:user,username',
            // 'password' => 'required'
        ]);

        $user = User::create([
            'username' => $validated['nisn'],
            'password' => bcrypt($validated['nisn'] . str_replace('-', '', $validated['tanggal-lahir'])),
            'role' => 'Siswa'
        ]);

        Biodata::create([
            'nik' => $validated['nik'],
            'nama_lengkap' => $validated['nama-lengkap'],
            'tanggal_lahir' => $validated['tanggal-lahir'],
            'tempat_lahir' => $validated['tempat-lahir'],
            'alamat' => $validated['alamat'],
            'jenis_kelamin' => $validated['jenis-kelamin'],
            'id_user' => $user->id
        ]);

        Siswa::create([
            'nisn' => $validated['nisn'],
            'nik' => $validated['nik'],
            'npsn' => $request->user()->sekolah->npsn
        ]);

        return redirect(route('siswa'));
    }

    function siswaDelete($nik)
    {
        Siswa::where('nik', '=', $nik)->delete();
        $biodata = Biodata::find($nik);
        $id_user = $biodata->id_user;
        $biodata->delete();
        User::find($id_user)->delete();

        return redirect(route('siswa'));
    }
}
