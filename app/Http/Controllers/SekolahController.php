<?php

namespace App\Http\Controllers;

use App\Models\Biodata;
use App\Models\PenanggungJawabSekolah;
use App\Models\Sekolah;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class SekolahController extends Controller
{
    function sekolah()
    {
        $data['sekolah'] = Sekolah::withExists(['siswa', 'periodeMagang', 'penanggungJawabSekolah'])->get();
        return view('sekolah', $data);
    }

    function exportPdf()
    {
        $data['sekolah'] = Sekolah::with('penanggungJawabSekolah.biodata')->get();
        // return view('pdf.sekolah', $data);
        return Pdf::loadView('pdf.sekolah', $data)->download('Data Sekolah (' . Carbon::now() . ').pdf');
    }

    function sekolahPost(Request $request)
    {
        $validated = $request->validate([
            'npsn' => 'required|numeric|digits:8|unique:sekolah,npsn',
            'nama-sekolah' => 'required',
            'username' => 'required|unique:user,username',
            'password' => 'required'
        ]);

        $user = User::create([
            'username' => $validated['username'],
            'password' => bcrypt($validated['password']),
            'role' => 'Sekolah'
        ]);

        Sekolah::create([
            'npsn' => $validated['npsn'],
            'nama_sekolah' => $validated['nama-sekolah'],
            'id_user' => $user->id
        ]);

        return redirect(route('sekolah'));
    }

    function sekolahPut(Request $request)
    {
        $validated = $request->validate([
            'npsn-baru' => 'required|numeric|digits:8|unique:sekolah,npsn,' . $request['npsn-lama'] . ',npsn',
            'nama-sekolah-baru' => 'required',
        ]);

        Sekolah::find($request['npsn-lama'])->update([
            'npsn' => $validated['npsn-baru'],
            'nama_sekolah' => $validated['nama-sekolah-baru']
        ]);

        return redirect(route('sekolah'));
    }

    function sekolahDelete($npsn)
    {
        $sekolah = Sekolah::find($npsn);
        $id_user = $sekolah->id_user;
        $sekolah->delete();
        User::find($id_user)->delete();
        return redirect(route('sekolah'));
    }

    function penanggungJawab(Request $request)
    {
        $user = $request->user();
        if ($user->role == 'Sekolah') {
            $data['penanggungJawab'] = PenanggungJawabSekolah::with('biodata')->where('npsn', $user->sekolah->npsn)->get();
        } else {
            $sekolah = Sekolah::with('penanggungJawabSekolah.biodata')->find($request->route()->parameter('npsn'));
            $data['namaSekolah'] = $sekolah->nama_sekolah;
            $data['penanggungJawab'] = $sekolah->penanggungJawabSekolah;
        }

        return view('penanggung-jawab-sekolah', $data);
    }

    function penanggungJawabPost(Request $request)
    {
        $validated = $request->validate([
            'no-telp' => 'required|numeric|unique:biodata,no_telp',
            'nik' => 'required|numeric|digits:16|unique:biodata,nik',
            'nama-lengkap' => 'required',
            'tempat-lahir' => 'required',
            'tanggal-lahir' => 'required|date_format:Y-m-d',
            'alamat' => 'required',
            'jenis-kelamin' => 'required|in:Laki - laki,Perempuan'
        ]);

        $biodata = Biodata::create([
            'nik' => $validated['nik'],
            'nama_lengkap' => $validated['nama-lengkap'],
            'tanggal_lahir' => $validated['tanggal-lahir'],
            'tempat_lahir' => $validated['tempat-lahir'],
            'alamat' => $validated['alamat'],
            'jenis_kelamin' => $validated['jenis-kelamin'],
            'no_telp' => $validated['no-telp']
        ]);

        PenanggungJawabSekolah::create([
            'nik' => $biodata->nik,
            'npsn' => $request->user()->sekolah->npsn,
        ]);

        return redirect(route('penanggungJawabSekolah'));
    }

    function penanggungJawabDelete($nik)
    {
        PenanggungJawabSekolah::destroy($nik);
        Biodata::destroy($nik);

        return redirect(route('penanggungJawabSekolah'));
    }
}
