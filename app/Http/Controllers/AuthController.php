<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    function login()
    {
        return view('login');
    }

    function loginPost(Request $request)
    {
        $validated = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
        if(Auth::attemptWhen($validated, function (User $user) {
            return match ($user->role) {
                'Admin' => $user->biodata()->exists(),
                'Petugas Biro' => $user->biodata()->exists(),
                'Sekolah' => $user->sekolah()->exists(),
                'Siswa' => $user->biodata->siswa()->exists(),
                default => false
            };
        })) {
            $request->session()->regenerate();
            return redirect(route('dashboard'));
        }
        return back()->withErrors([
            'credentials' => 'The provided credentials do not match our records.',
        ]);
    }

    function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect(route('login'));
    }
}
