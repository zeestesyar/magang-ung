<?php

namespace App\Http\Controllers;

use App\Models\Biodata;
use App\Models\Biro;
use App\Models\PenanggungJawabBiro;
use App\Models\SiswaMagang;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class BiroController extends Controller
{
    function biro()
    {
        $data['biro'] = Biro::withExists('siswaMagang')->get();
        return view('biro', $data);
    }

    function exportPdf()
    {
        $data['biro'] = Biro::with('penanggungJawabBiro.biodata')->get();
        // return view('pdf.biro', $data);
        return Pdf::loadView('pdf.biro', $data)->download('Data Biro (' . Carbon::now() . ').pdf');
    }

    function biroPost(Request $request)
    {
        $validated = $request->validate([
            'nama-biro' => 'required'
        ]);

        Biro::create([
            'nama_biro' => $validated['nama-biro']
        ]);

        return redirect(route('biro'));
    }

    function biroPut(Request $request)
    {
        $validated = $request->validate([
            'nama-biro-baru' => 'required'
        ]);

        Biro::find($request['id-biro'])->update([
            'nama_biro' => $validated['nama-biro-baru']
        ]);

        return redirect(route('biro'));
    }

    function biroDelete($id)
    {
        Biro::find($id)->delete();
        return redirect(route('biro'));
    }

    function penanggungJawab(Request $request)
    {
        $idBiro = $request->route()->parameter('id');
        $biro = Biro::with('penanggungJawabBiro.biodata')->find($idBiro);
        $namaBiro = $biro->nama_biro;
        $penanggungJawab = $biro->penanggungJawabBiro;
        return view('penanggung-jawab-biro', compact('idBiro', 'namaBiro', 'penanggungJawab'));
    }

    function penanggungJawabPost(Request $request)
    {
        $validated = $request->validate([
            'no-telp' => 'required|numeric|unique:biodata,no_telp',
            'nik' => 'required|numeric|digits:16|unique:biodata,nik',
            'nama-lengkap' => 'required',
            'tempat-lahir' => 'required',
            'tanggal-lahir' => 'required|date_format:Y-m-d',
            'alamat' => 'required',
            'jenis-kelamin' => 'required|in:Laki - laki,Perempuan'
        ]);

        $biodata = Biodata::create([
            'nik' => $validated['nik'],
            'nama_lengkap' => $validated['nama-lengkap'],
            'tanggal_lahir' => $validated['tanggal-lahir'],
            'tempat_lahir' => $validated['tempat-lahir'],
            'alamat' => $validated['alamat'],
            'jenis_kelamin' => $validated['jenis-kelamin'],
            'no_telp' => $validated['no-telp']
        ]);

        PenanggungJawabBiro::create([
            'nik' => $biodata->nik,
            'id_biro' => $request['id-biro']
        ]);

        return redirect(route('penanggungJawabBiro', $request['id-biro']));
    }

    function penanggungJawabDelete($idBiro, $nik)
    {
        PenanggungJawabBiro::find($nik)->delete();
        Biodata::find($nik)->delete();

        return redirect(route('penanggungJawabBiro', $idBiro));
    }

    function siswaMagang(Request $request)
    {
        $biro = Biro::with(['siswaMagang.periodeMagang.sekolah', 'siswaMagang.siswa.biodata'])->get();
        if (is_null($request['biro'])) {
            $table = $biro;
        } else if (is_null($request['id-periode'])) {
            $table = Biro::with(['siswaMagang.periodeMagang.sekolah', 'siswaMagang.siswa.biodata'])->where('id', $request['biro'])->get();
        } else {
            $table = Biro::with([
                'siswaMagang.periodeMagang.sekolah',
                'siswaMagang.siswa.biodata',
                'siswaMagang.periodeMagang' => function ($query) use ($request) {
                    $query->where('id', $request['id-periode']);
                }
            ])->where('id', $request['biro'])->get();
        }

        return view('siswa-magang', compact('biro', 'table'));
    }

    function siswaMagangExportPdf(Request $request)
    {
        if (is_null($request['biro'])) {
            $biro = Biro::with(['siswaMagang.periodeMagang.sekolah', 'siswaMagang.siswa.biodata'])->get();
        } else if (is_null($request['id-periode'])) {
            $biro = Biro::with(['siswaMagang.periodeMagang.sekolah', 'siswaMagang.siswa.biodata'])->where('id', $request['biro'])->get();
        } else {
            $biro = Biro::with([
                'siswaMagang.periodeMagang.sekolah',
                'siswaMagang.siswa.biodata',
                'siswaMagang.periodeMagang' => function ($query) use ($request) {
                    $query->where('id', $request['id-periode']);
                }
            ])->where('id', $request['biro'])->get();
        }
        // return view('pdf.siswa-magang', compact('biro'));
        return Pdf::loadView('pdf.siswa-magang', compact('biro'))->download('Data Siswa Magang (' . Carbon::now() . ').pdf');
    }

    function penilaianSiswaMagang(Request $request)
    {
        $validated = $request->validate([
            'nilai' => 'required|numeric|max:100'
        ]);
        SiswaMagang::find($request['nisn'])->update([
            'penilaian' => $validated['nilai']
        ]);
        return redirect(route('siswaMagang'));
    }
}
