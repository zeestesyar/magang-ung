@extends('layouts.template')

@section('title', 'Siswa')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Siswa</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Siswa</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>NISN</th>
                                            <th>NIK</th>
                                            <th>Nama Lengkap</th>
                                            <th>Tempat/Tanggal Lahir</th>
                                            <th>Alamat</th>
                                            <th>Jenis Kelamin</th>
                                            <th><button type="button" class="btn btn-default" data-toggle="modal"
                                                    data-target="#modal-lg">
                                                    Tambah
                                                </button></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($siswa as $s)
                                            <tr>
                                                <td>{{ $s->nisn }}</td>
                                                <td>{{ $s->biodata->nik }}</td>
                                                <td>{{ $s->biodata->nama_lengkap }}</td>
                                                <td>{{ $s->biodata->tempat_lahir . ', ' . $s->biodata->tanggal_lahir }}</td>
                                                <td>{{ $s->biodata->alamat }}</td>
                                                <td>{{ $s->biodata->jenis_kelamin }}</td>
                                                <td><button type="button" class="btn btn-danger"
                                                        onclick="hapusSiswa('{{ $s->biodata->nama_lengkap }}', '{{ $s->biodata->nik }}')" @if ($s->siswa_magang_exists)
                                                            disabled
                                                        @endif>
                                                        Hapus
                                                    </button></td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Siswa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('siswaPost') }}" method="post">
                    @csrf
                    <div class="modal-body">

                        <div class="input-group mb-3">
                            <input type="text"
                                class="form-control @error('nik')
                            is-invalid
                        @enderror"
                                placeholder="NIK" name="nik" value="{{ old('nik') }}">
                            @error('nik')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="input-group mb-3">
                            <input type="text"
                                class="form-control @error('nisn')
                            is-invalid
                        @enderror"
                                placeholder="NISN" name="nisn" value="{{ old('nisn') }}">
                            @error('nisn')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="input-group mb-3">
                            <input type="text"
                                class="form-control @error('nama-lengkap')
                            is-invalid
                        @enderror"
                                placeholder="Nama Lengkap" name="nama-lengkap" value="{{ old('nama-lengkap') }}">
                            @error('nama-lengkap')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="input-group mb-3">
                            <input type="text"
                                class="form-control @error('tempat-lahir')
                            is-invalid
                        @enderror"
                                placeholder="Tempat Lahir" name="tempat-lahir" value="{{ old('tempat-lahir') }}">
                            @error('tempat-lahir')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="input-group date mb-3" id="reservationdate" data-target-input="nearest">
                            <input type="text"
                                class="form-control datetimepicker-input @error('tanggal-lahir')
                            is-invalid
                        @enderror"
                                data-target="#reservationdate" placeholder="Tanggal Lahir" name="tanggal-lahir" value="{{ old('tanggal-lahir') }}" />
                            <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                            @error('tanggal-lahir')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="input-group mb-3">
                            <input type="text"
                                class="form-control @error('alamat')
                            is-invalid
                        @enderror"
                                placeholder="Alamat" name="alamat" value="{{ old('alamat') }}">
                            @error('alamat')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="input-group mb-3 clearfix">
                            <div class="icheck-primary d-inline mr-2">
                                <input type="radio" id="radioPrimary1"
                                    name="jenis-kelamin" value="Laki - laki" @if (old('jenis-kelamin') == 'Laki - laki')
                                    checked
                                @endif>
                                <label for="radioPrimary1">
                                    Laki - laki
                                </label>
                            </div>
                            <div class="icheck-primary d-inline">
                                <input type="radio" id="radioPrimary2"
                                    name="jenis-kelamin" value="Perempuan" @if (old('jenis-kelamin') == 'Perempuan')
                                    checked
                                @endif>
                                <label for="radioPrimary2">
                                    Perempuan
                                </label>
                            </div>
                            @error('jenis-kelamin')
                                <div class="invalid-feedback d-block" style="">{{ $message }}</div>
                            @enderror
                        </div>

                        {{-- <div class="input-group mb-3">
                            <input type="text"
                                class="form-control @error('username')
                            is-invalid
                        @enderror"
                                placeholder="Username" name="username" value="{{ old('username') }}">
                            @error('username')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="input-group mb-3">
                            <input type="password"
                                class="form-control @error('password')
                            is-invalid
                        @enderror"
                                placeholder="Password" name="password">
                            @error('password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div> --}}
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <div class="modal fade" id="modal-sm">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Hapus Sekolah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-hapus-body">

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <a class="btn btn-primary" id="hapus-btn">Hapus</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection

@section('script')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        $(function() {
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
            $('#reservationdate').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        });
        @if ($errors->any())
            $('#modal-lg').modal('show');
        @endif

        function hapusSiswa(namaLengkap, nik) {
            $('#modal-hapus-body').html(`Anda yakin ingin menghapus siswa "${namaLengkap} (${nik})"`)
            $('#hapus-btn').attr('href', `/siswa/${nik}/delete`)
            $('#modal-sm').modal('show');
        }
    </script>
@endsection
