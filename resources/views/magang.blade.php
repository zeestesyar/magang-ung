@extends('layouts.template')

@section('title', 'Pengajuan Magang')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Magang</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Pengajuan Magang</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                                    <form action="{{ route('magang') }}" method="get">
                                        <div class="form-group">
                                            <label>Sekolah</label>
                                            <select class="form-control select2" style="width: 100%;" id="sekolah-select">
                                                <option id="sekolah-option">Pilih Sekolah</option>
                                                @foreach ($sekolah as $s)
                                                    <option value="{{ $s->npsn }}">
                                                        {{ $s->nama_sekolah }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary mb-3 mr-1">Cari</button>
                                        <a href="{{ route('pengajuanExportPdf', request()->query()) }}"
                                            class="btn btn-primary mb-3 mr-1">
                                            PDF
                                        </a>
                                    </form>
                                @endif

                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Waktu</th>
                                            <th>Surat</th>
                                            <th>Siswa</th>
                                            @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                                                <th>Sekolah</th>
                                            @endif
                                            <th>Status Persetujuan</th>
                                            @if (Auth::user()->role == 'Sekolah')
                                                <th><button type="button" class="btn btn-default" data-toggle="modal"
                                                        data-target="#modal-lg">
                                                        Tambah
                                                    </button></th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (Auth::user()->role == 'Sekolah')
                                            @foreach ($periodeMagang as $pm)
                                                <tr>
                                                    <td>{{ $pm->tmt . ' - ' . $pm->tst }}</td>
                                                    <td><a class="btn btn-primary" href="{{ $pm->url_surat }}">Lihat
                                                            Surat</a>
                                                    </td>
                                                    <td>
                                                        <ul>
                                                            @foreach ($pm->siswaMagang as $sm)
                                                                <li>{{ $sm->siswa->biodata->nama_lengkap }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </td>
                                                    @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                                                        <td>{{ $pm->sekolah->nama_sekolah }}</td>
                                                    @endif
                                                    <td>
                                                        @if (is_null($pm->sts_persetujuan))
                                                            <span class="badge bg-primary">Belum diproses</span>
                                                        @elseif($pm->sts_persetujuan)
                                                            <span class="badge bg-success">Disetujui</span>
                                                        @else
                                                            <button type="button" class="btn btn-danger"
                                                                onclick="lihatAlasan('{{ $pm->alasan }}')"><b>Ditolak</b>
                                                                (Lihat Alasan)
                                                                </a>
                                                        @endif

                                                    </td>
                                                    <td><button type="button" class="btn btn-danger"
                                                            onclick="hapusPengajuan('{{ $pm->tmt . ' - ' . $pm->tst }}', '{{ $pm->id }}')"
                                                            @if ($pm->sts_persetujuan) disabled @endif>
                                                            Hapus
                                                        </button></td>
                                                </tr>
                                            @endforeach
                                        @else
                                            @foreach ($table as $s)
                                                @foreach ($s->periodeMagang as $pm)
                                                    <tr>
                                                        <td>{{ $pm->tmt . ' - ' . $pm->tst }}</td>
                                                        <td><a class="btn btn-primary" href="{{ $pm->url_surat }}">Lihat
                                                                Surat</a>
                                                        </td>
                                                        <td>
                                                            <ul>
                                                                @foreach ($pm->siswaMagang as $sm)
                                                                    <li>{{ $sm->siswa->biodata->nama_lengkap }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </td>
                                                        <td>{{ $s->nama_sekolah }}</td>
                                                        <td>
                                                            @if (is_null($pm->sts_persetujuan))
                                                                <a class="btn btn-primary"
                                                                    href="{{ route('verifMagang', $pm->id) }}">Setuju</a>
                                                                <button type="button" class="btn btn-danger"
                                                                    onclick="tolak('{{ $pm->id }}')">Tolak</button>
                                                            @elseif($pm->sts_persetujuan)
                                                                <span class="badge bg-success">Disetujui</span>
                                                            @else
                                                                <button type="button" class="btn btn-danger"
                                                                    onclick="lihatAlasan('{{ $pm->alasan }}')"><b>Ditolak</b>
                                                                    (Lihat Alasan)
                                                                    </a>
                                                            @endif

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        @endif


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    <div class="modal fade" id="modal-alasan">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Alasan Penolakan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <textarea class="form-control" rows="3" id="alasan-textarea" readonly></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    @if (in_array(Auth::user()->role, ['Sekolah']))
        <div class="modal fade" id="modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('pengajuanMagang') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="far fa-calendar-alt"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control float-right" id="reservation"
                                    name="jangka-waktu">
                                @error('jangka-waktu')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="exampleInputFile" name="file"
                                        accept="application/pdf">
                                    <label class="custom-file-label" for="exampleInputFile">Pilih file</label>
                                </div>
                                @error('file')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>

                            <div
                                class="form-group @error('nisn')
                            is-invalid
                        @enderror">
                                <select class="select2" multiple="multiple" data-placeholder="Pilih siswa"
                                    style="width: 100%;" name="nisn[]">
                                    @foreach ($siswa as $s)
                                        <option value="{{ $s->nisn }}">
                                            {{ $s->biodata->nama_lengkap . ' (' . $s->biodata->nik . ')' }}</option>
                                    @endforeach
                                </select>
                                @error('nisn')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="modal fade" id="modal-sm">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Hapus Sekolah</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="modal-hapus-body">

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <a class="btn btn-primary" id="hapus-btn">Hapus</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    @else
        <div class="modal fade" id="modal-submit-tolak">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Tolak Pengajuan</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('tolakMagang') }}" method="post">
                        @csrf
                        <input type="hidden" name="id-pengajuan" id="id-pengajuan-input">
                        <div class="modal-body">
                            <div class="form-group">
                                <textarea class="form-control" rows="3" placeholder="Masukkan alasan jika ada..." name="alasan"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Tolak</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    @endif
@endsection

@section('script')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>

    <script>
        $(function() {
            $('.select2').select2({
                theme: 'bootstrap4'
            })

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
            @if (in_array(Auth::user()->role, ['Sekolah']))
                $('#reservation').daterangepicker();
                $('#reservation').val('')
                $('.select2').select2();
                // $('#exampleInputFile').change(() {

                // })
            @endif

            @if (in_array(Auth::user()->role, ['Sekolah']))
                @if ($errors->any())
                    $('#modal-lg').modal('show');
                @endif
                function hapusPengajuan(waktu, id) {
                    $('#modal-hapus-body').html(`Anda yakin ingin menghapus pengajuan "${waktu}"`)
                    $('#hapus-btn').attr('href', `/magang/pengajuan/${id}/delete`)
                    $('#modal-sm').modal('show');
                }
            @endif



            $('#sekolah-select').on('change', function() {
                $(this).attr('name', 'sekolah')
                $('#sekolah-option').attr('disabled', true)
            })
        });

        function tolak(id) {
            $('#id-pengajuan-input').val(id)
            $('#modal-submit-tolak').modal('show')
        }

        function lihatAlasan(alasan) {
            $('#alasan-textarea').val(alasan)
            $('#modal-alasan').modal('show')
        }
    </script>
@endsection
