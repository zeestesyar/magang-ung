@extends('pdf.layouts.template')

@section('header', 'Data Biro')

@section('table')
    <table>
        <thead>
            <tr>
                <th rowspan="2" style="width: 10px">No</th>
                <th rowspan="2">Nama Biro</th>
                <th colspan="4">Penanggung Jawab</th>
            </tr>
            <tr>
                <th>NIK</th>
                <th>Nama Lengkap</th>
                <th>Jenis Kelamin</th>
                <th>Nomor Telepon</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($biro as $index => $data)
                @if ($data->penanggungJawabBiro->count() == 0)
                    <tr>
                        <td style="text-align: center">{{ ++$index }}</td>
                        <td>{{ $data->nama_biro }}</td>
                        <td colspan="4" style="text-align: center">Belum ada Penanggung Jawab</td>
                    </tr>
                @else
                    @foreach ($data->penanggungJawabBiro as $pji => $pj)
                        <tr>
                            @if ($pji == 0)
                                <td rowspan="{{ $data->penanggungJawabBiro->count() }}" style="text-align: center">
                                    {{ ++$index }}</td>
                                <td rowspan="{{ $data->penanggungJawabBiro->count() }}">{{ $data->nama_biro }}</td>
                            @endif
                            <td>{{ $pj->biodata->nik }}</td>
                            <td>{{ $pj->biodata->nama_lengkap }}</td>
                            <td>{{ $pj->biodata->jenis_kelamin }}</td>
                            <td>{{ $pj->biodata->no_telp }}</td>
                        </tr>
                    @endforeach
                @endif
            @endforeach
        </tbody>
    </table>
@endsection
