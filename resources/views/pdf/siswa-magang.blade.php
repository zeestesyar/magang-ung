@extends('pdf.layouts.template')

@section('header', 'Data Siswa Magang')

@section('table')
    <table>
        <thead>
            <tr>
                <th rowspan="2" style="width: 10px">No</th>
                <th rowspan="2">Biro</th>
                <th colspan="2">Periode</th>
                <th rowspan="2">Sekolah</th>
                <th rowspan="2">Siswa</th>
                <th rowspan="2">Penilaian</th>
            </tr>
            <tr>
                <th>Tanggal Mulai</th>
                <th>Tanggal Selesai</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($biro as $bi => $b)
                @php
                    $count = 0;
                    $row = $b->siswaMagang
                        ->filter(function ($item, $index) {
                            return !is_null($item->periodeMagang);
                        })
                        ->count();
                @endphp
                @foreach ($b->siswaMagang as $smi => $sm)
                    @if (!is_null($sm->periodeMagang))
                        <tr>
                            @if ($count++ == 0)
                                <td rowspan="{{ $row }}">{{ ++$bi }}</td>
                                <td rowspan="{{ $row }}">{{ $b->nama_biro }}</td>
                            @endif

                            <td>{{ $sm->periodeMagang->tmt }}</td>
                            <td>{{ $sm->periodeMagang->tst }}</td>
                            <td>{{ $sm->periodeMagang->sekolah->nama_sekolah }}</td>
                            <td>{{ $sm->siswa->biodata->nama_lengkap }}</td>
                            @if (is_null($sm->penilaian))
                                <td>Belum ada Penilaian</td>
                            @else
                                <td>{{ $sm->penilaian }}</td>
                            @endif
                        </tr>
                        @php
                            $count++;
                        @endphp
                    @endif
                @endforeach
            @endforeach
        </tbody>
    </table>
@endsection
