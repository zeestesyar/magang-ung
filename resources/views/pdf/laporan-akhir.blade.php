@extends('pdf.layouts.template')

@section('header', 'Data Laporan Akhir')

@section('table')
    <table>
        <thead>
            <tr>
                <th rowspan="2" style="width: 10px">No</th>
                <th rowspan="2">Sekolah</th>
                <th colspan="2">Periode</th>
                <th rowspan="2">Siswa</th>

            </tr>
            <tr>
                <th>Tanggal Mulai</th>
                <th>Tanggal Selesai</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($sekolah as $si => $s)
                @foreach ($s->periodeMagang as $pmi => $pm)
                    @foreach ($pm->siswaMagang as $smi => $sm)
                        <tr>
                            @php
                                $sRow = $s->periodeMagang->map(function($item, $index) {
                                    return $item->siswaMagang->count();
                                })->sum();
                            @endphp
                            @if ($pmi == 0 && $smi == 0)
                            <td rowspan="{{ $sRow }}">{{ ++$si }}</td>
                            <td rowspan="{{ $sRow }}">{{ $s->nama_sekolah }}</td>
                            @endif

                            @if ($smi == 0)
                                <td rowspan="{{ $pm->siswaMagang->count() }}">{{ $pm->tmt }}</td>
                                <td rowspan="{{ $pm->siswaMagang->count() }}">{{ $pm->tst }}</td>
                            @endif

                            <td>{{ $sm->siswa->biodata->nama_lengkap }}</td>
                        </tr>
                    @endforeach
                @endforeach
            @endforeach
        </tbody>
    </table>
@endsection
