@extends('pdf.layouts.template')

@section('header', 'Data Sekolah')

@section('table')
    <table>
        <thead>
            <tr>
                <th style="width: 10px" rowspan="2">No</th>
                <th rowspan="2">NPSN</th>
                <th rowspan="2">Nama Sekolah</th>
                <th colspan="4">Penanggung Jawab</th>
            </tr>
            <tr>
                <th>NIK</th>
                <th>Nama Lengkap</th>
                <th>Jenis Kelamin</th>
                <th>Nomor Telepon</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($sekolah as $index => $data)
                @if ($data->penanggungJawabSekolah->count() == 0)
                    <td style="text-align: center">
                        {{ ++$index }}</td>
                    <td>{{ $data->npsn }}</td>
                    <td>{{ $data->nama_sekolah }}</td>
                    <td colspan="4" style="text-align: center">Belum ada Penanggung Jawab</td>
                @else
                    @foreach ($data->penanggungJawabSekolah as $pji => $pj)
                        <tr>
                            @if ($pji == 0)
                                <td rowspan="{{ $data->penanggungJawabSekolah->count() }}" style="text-align: center">
                                    {{ ++$index }}</td>
                                <td rowspan="{{ $data->penanggungJawabSekolah->count() }}">{{ $data->npsn }}</td>
                                <td rowspan="{{ $data->penanggungJawabSekolah->count() }}">{{ $data->nama_sekolah }}</td>
                            @endif
                            <td>{{ $pj->biodata->nik }}</td>
                            <td>{{ $pj->biodata->nama_lengkap }}</td>
                            <td>{{ $pj->biodata->jenis_kelamin }}</td>
                            <td>{{ $pj->biodata->no_telp }}</td>
                        </tr>
                    @endforeach
                @endif
            @endforeach
        </tbody>
    </table>
@endsection
