@extends('pdf.layouts.template')

@section('header', 'Data Pengajuan')

@section('table')
    <table>
        <thead>
            <tr>
                <th rowspan="2" style="width: 10px">No</th>
                <th rowspan="2">Sekolah</th>
                <th colspan="2">Periode</th>
                <th rowspan="2">Status Persetujuan</th>
                <th rowspan="2">Siswa</th>

            </tr>
            <tr>
                <th>Tanggal Mulai</th>
                <th>Tanggal Selesai</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($sekolah as $si => $s)
                @foreach ($s->periodeMagang as $pmi => $pm)
                    @foreach ($pm->siswaMagang as $smi => $sm)
                        <tr>
                            @if ($pmi == 0 && $smi == 0)
                                <td
                                    rowspan="{{ $s->periodeMagang->map(function ($item, $index) {
                                            return $item->siswaMagang->count();
                                        })->sum() }}" style="text-align: center">
                                    {{ ++$si }}</td>
                                <td
                                    rowspan="{{ $s->periodeMagang->map(function ($item, $index) {
                                            return $item->siswaMagang->count();
                                        })->sum() }}">
                                    {{ $s->nama_sekolah . ' (' . $s->npsn . ')' }}</td>
                            @endif
                            @if ($smi == 0)
                                <td rowspan="{{ $pm->siswaMagang->count() }}">{{ $pm->tmt }}</td>
                                <td rowspan="{{ $pm->siswaMagang->count() }}">{{ $pm->tst }}</td>
                                <td rowspan="{{ $pm->siswaMagang->count() }}">
                                    @if (is_null($pm->sts_persetujuan))
                                        Belum diproses
                                    @elseif($pm->sts_persetujuan)
                                        Disetujui
                                    @else
                                        Ditolak
                                    @endif
                                </td>
                            @endif
                            <td>{{ $sm->siswa->biodata->nama_lengkap . ' (' . $sm->siswa->nisn . ')' }}</td>

                        </tr>
                    @endforeach
                @endforeach
            @endforeach
        </tbody>
    </table>
@endsection
