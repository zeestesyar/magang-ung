<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF</title>
    <style>
        table,
        th,
        td {
            border: 1px solid;
            border-color: black;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        th {
            background-color: maroon;
            color: white;
            padding: 15px;
            text-align: center;
        }

        td {
            padding-left: 15px;
            padding-right: 15px;
            padding-top: 8px;
            padding-bottom: 8px;
            text-align: left;
        }

        h2 {
            text-align: center;
        }
    </style>
</head>

<body>
    <h2>@yield('header')</h2>
    @yield('table')
</body>

</html>
