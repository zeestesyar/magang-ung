@extends('pdf.layouts.template')

@section('header', 'Data Jurnal')

@section('table')
    <table>
        <thead>
            <tr>
                <th rowspan="2" style="width: 10px">No</th>
                <th rowspan="2">Sekolah</th>
                <th colspan="2">Periode</th>
                <th rowspan="2">Siswa</th>
                <th rowspan="2">Tanggal</th>
                <th rowspan="2">Total Kegiatan</th>

            </tr>
            <tr>
                <th>Tanggal Mulai</th>
                <th>Tanggal Selesai</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($sekolah as $si => $s)
                @foreach ($s->periodeMagang as $pmi => $pm)
                    @foreach ($pm->siswaMagang as $smi => $sm)
                        @foreach ($sm->kehadiranSiswa as $ksi => $ks)
                            <tr>
                                @php
                                    $sRow = $s->periodeMagang
                                        ->map(function ($item, $index) {
                                            return $item->siswaMagang
                                                ->map(function ($item, $index) {
                                                    return $item->kehadiranSiswa->count();
                                                })
                                                ->sum();
                                        })
                                        ->sum();
                                @endphp
                                @if ($pmi == 0 && $smi == 0 && $ksi == 0)
                                    <td rowspan="{{ $sRow }}">{{ ++$si }}</td>
                                    <td rowspan="{{ $sRow }}">{{ $s->nama_sekolah }}</td>
                                @endif

                                @php
                                    $pmRow = $pm->siswaMagang
                                        ->map(function ($item, $index) {
                                            return $item->kehadiranSiswa->count();
                                        })
                                        ->sum();
                                @endphp
                                @if ($smi == 0 && $ksi == 0)
                                    <td rowspan="{{ $pmRow }}">{{ $pm->tmt }}</td>
                                    <td rowspan="{{ $pmRow }}">{{ $pm->tst }}</td>
                                @endif

                                @if ($ksi == 0)
                                    <td rowspan="{{ $sm->kehadiranSiswa->count() }}">
                                        {{ $sm->siswa->biodata->nama_lengkap }}
                                    </td>
                                @endif

                                <td>{{ $ks->tanggal }}</td>
                                <td>{{ $ks->kegiatanSiswa->count() }}</td>
                            </tr>
                        @endforeach
                    @endforeach
                @endforeach
            @endforeach
        </tbody>
    </table>
@endsection
