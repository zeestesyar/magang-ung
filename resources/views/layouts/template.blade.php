<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{ asset('ung.png') }}" type="image/png">
    <link rel="shortcut icon" href="{{ asset('ung.png') }}" type="image/png">
    <title>Monitoring Magang | @yield('title')</title>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    @yield('css')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i
                            class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">

                <!-- Messages Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" href="{{ route('logout') }}">
                        <i class="fa fa-arrow-left mr-1"></i><b>Logout</b>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
                <img src="{{ asset('ung.png') }}" alt="Magang UNG" class="brand-image img-circle elevation-3"
                    style="opacity: .8">
                <span class="brand-text font-weight-light">Magang UNG</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">

                    <div class="info">
                        @switch(Auth::user()->role)
                            @case('Admin')
                                <a href="#" class="d-block">{{ Auth::user()->biodata->nama_lengkap . ' (Admin)' }}</a>
                            @break

                            @case('Petugas Biro')
                                <a href="#"
                                    class="d-block">{{ Auth::user()->biodata->nama_lengkap . ' (Petugas Biro)' }}</a>
                            @break

                            @case('Siswa')
                                <a href="#" class="d-block">{{ Auth::user()->biodata->nama_lengkap . ' (Siswa)' }}</a>
                            @break

                            @case('Sekolah')
                                <a href="#" class="d-block">{{ Auth::user()->sekolah->nama_sekolah . ' (Sekolah)' }}</a>
                            @break
                        @endswitch
                    </div>
                </div>

                <!-- SidebarSearch Form -->
                <div class="form-inline">
                    <div class="input-group" data-widget="sidebar-search">
                        <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                            aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-sidebar">
                                <i class="fas fa-search fa-fw"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <li class="nav-item">
                            <a href="{{ route('dashboard') }}"
                                class="nav-link @if (request()->routeIs('dashboard')) active @endif">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                </p>
                            </a>
                        </li>
                        @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro', 'Sekolah']))
                            <li class="nav-item @if (request()->routeIs(['sekolah', 'penanggungJawabSekolah', 'daftarPenanggungJawabSekolah'])) menu-open @endif">
                                <a href="#" class="nav-link @if (request()->routeIs(['sekolah', 'penanggungJawabSekolah', 'daftarPenanggungJawabSekolah'])) active @endif">
                                    <i class="nav-icon fas fa-school"></i>
                                    <p>
                                        Sekolah
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                                        <li class="nav-item">
                                            <a href="{{ route('sekolah') }}"
                                                class="nav-link @if (request()->routeIs(['sekolah', 'daftarPenanggungJawabSekolah'])) active @endif">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Daftar</p>
                                            </a>
                                        </li>
                                    @endif
                                    @if (in_array(Auth::user()->role, ['Sekolah']))
                                        <li class="nav-item">
                                            <a href="{{ route('penanggungJawabSekolah') }}"
                                                class="nav-link @if (request()->routeIs('penanggungJawabSekolah')) active @endif">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Penanggung Jawab</p>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif

                        {{-- <li class="nav-item">
                            <a href="{{ route('sekolah') }}"
                                class="nav-link @if (request()->routeIs('sekolah')) active @endif">
                                <i class="nav-icon fas fa-school"></i>
                                <p>
                                    Sekolah
                                </p>
                            </a>
                        </li> --}}

                        @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                            <li class="nav-item @if (request()->routeIs(['biro', 'penanggungJawabBiro', 'siswaMagang'])) menu-open @endif">
                                <a href="#" class="nav-link @if (request()->routeIs(['biro', 'penanggungJawabBiro', 'siswaMagang'])) active @endif">
                                    <i class="nav-icon fas fa-building"></i>
                                    <p>
                                        Biro
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{ route('biro') }}"
                                            class="nav-link @if (request()->routeIs(['biro', 'penanggungJawabBiro'])) active @endif">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Daftar</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('siswaMagang') }}"
                                            class="nav-link @if (request()->routeIs(['siswaMagang'])) active @endif">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Siswa Magang</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            {{-- <li class="nav-item">
                                <a href="{{ route('biro') }}"
                                    class="nav-link @if (request()->routeIs(['biro', 'penanggungJawabBiro'])) active @endif">
                                    <i class="nav-icon fas fa-building"></i>
                                    <p>
                                        Biro
                                    </p>
                                </a>
                            </li> --}}
                        @endif
                        @if (in_array(Auth::user()->role, ['Admin']))
                            <li class="nav-item">
                                <a href="{{ route('petugasBiro') }}"
                                    class="nav-link @if (request()->routeIs('petugasBiro')) active @endif">
                                    <i class="nav-icon fas fa-user-tie"></i>
                                    <p>
                                        Petugas Biro
                                    </p>
                                </a>
                            </li>
                        @endif
                        @if (in_array(Auth::user()->role, ['Sekolah']))
                            <li class="nav-item">
                                <a href="{{ route('siswa') }}"
                                    class="nav-link @if (request()->routeIs('siswa')) active @endif">
                                    <i class="nav-icon fas fa-user"></i>
                                    <p>
                                        Siswa
                                    </p>
                                </a>
                            </li>
                        @endif
                        {{-- @if (in_array(Auth::user()->role, ['Sekolah', 'Petugas Biro', 'Admin', ''])) --}}
                        <li class="nav-item @if (request()->routeIs(['magang', 'distribusiSiswa', 'jurnalKegiatan', 'laporanAkhir'])) menu-open @endif">
                            <a href="#" class="nav-link @if (request()->routeIs(['magang', 'distribusiSiswa', 'jurnalKegiatan', 'laporanAkhir'])) active @endif">
                                <i class="nav-icon fas fa-edit"></i>
                                <p>
                                    Magang
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @if (in_array(Auth::user()->role, ['Sekolah', 'Petugas Biro', 'Admin']))
                                    <li class="nav-item">
                                        <a href="{{ route('magang') }}"
                                            class="nav-link @if (request()->routeIs('magang')) active @endif">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Pengajuan Magang</p>
                                        </a>
                                    </li>
                                @endif
                                @if (in_array(Auth::user()->role, ['Petugas Biro', 'Admin']))
                                    <li class="nav-item">
                                        <a href="{{ route('distribusiSiswa') }}"
                                            class="nav-link @if (request()->routeIs('distribusiSiswa')) active @endif">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Distribusi Siswa</p>
                                        </a>
                                    </li>
                                @endif
                                <li class="nav-item">
                                    <a href="{{ route('absensi') }}"
                                        class="nav-link @if (request()->routeIs('absensi')) active @endif">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Absensi</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('jurnalKegiatan') }}"
                                        class="nav-link @if (request()->routeIs('jurnalKegiatan')) active @endif">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Jurnal Kegiatan Harian</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('laporanAkhir') }}"
                                        class="nav-link @if (request()->routeIs('laporanAkhir')) active @endif">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Laporan Akhir</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        {{-- @endif --}}
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        @yield('content')

        {{-- <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 3.2.0
            </div>
            <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights
            reserved.
        </footer> --}}
    </div>


    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    @yield('script')
</body>

</html>
