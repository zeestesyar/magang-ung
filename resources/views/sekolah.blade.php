    @extends('layouts.template')

    @section('title', 'Sekolah')

    @section('css')
        <!-- DataTables -->
        <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    @endsection

    @section('content')
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Sekolah</h1>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <section class="content">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Sekolah</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <a href="{{ route('sekolahExportPdf') }}" class="btn btn-primary">
                                        PDF
                                    </a>
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>NPSN</th>
                                                <th>Nama Sekolah</th>
                                                <th><button type="button" class="btn btn-default" data-toggle="modal"
                                                        data-target="#modal-lg">
                                                        Tambah
                                                    </button></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($sekolah as $s)
                                                <tr>
                                                    <td>{{ $s->npsn }}</td>
                                                    <td>{{ $s->nama_sekolah }}</td>
                                                    <td><button type="button" class="btn btn-danger"
                                                            onclick="hapus('{{ $s->nama_sekolah }}', '{{ $s->npsn }}')"
                                                            @if ($s->siswa_exists || $s->periode_magang_exists || $s->penanggung_jawab_sekolah_exists) disabled @endif>
                                                            Hapus
                                                        </button>
                                                        <button type="button" class="btn btn-primary"
                                                            onclick="ubah('{{ $s->npsn }}', '{{ $s->nama_sekolah }}')">
                                                            Ubah
                                                        </button>
                                                        <a href="{{ route('daftarPenanggungJawabSekolah', $s->npsn) }}" class="btn btn-primary">
                                                            Penanggung Jawab
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
        </div>
        <div class="modal fade" id="modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah Sekolah</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('sekolahPost') }}" method="post">
                        @csrf
                        <div class="modal-body">
                            <div class="input-group mb-3">
                                <input type="text"
                                    class="form-control @error('npsn')
                                is-invalid
                            @enderror"
                                    placeholder="NPSN" name="npsn" value="{{ old('npsn') }}">
                                @error('npsn')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group mb-3">
                                <input type="text"
                                    class="form-control @error('nama-sekolah')
                                is-invalid
                            @enderror"
                                    placeholder="Nama Sekolah" name="nama-sekolah" value="{{ old('nama-sekolah') }}">
                                @error('nama-sekolah')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group mb-3">
                                <input type="text"
                                    class="form-control @error('username')
                                is-invalid
                            @enderror"
                                    placeholder="Username Sekolah" name="username" value="{{ old('username') }}">
                                @error('username')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group mb-3">
                                <input type="password"
                                    class="form-control @error('password')
                                is-invalid
                            @enderror"
                                    placeholder="Password Sekolah" name="password" value="{{ old('password') }}">
                                @error('password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="modal-update">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Ubah</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('sekolahPut') }}" method="post">
                        @csrf
                        <input type="hidden" name="npsn-lama" id="npsn-lama-input" value="{{ old('npsn-lama') }}">
                        <div class="modal-body">
                            <div class="input-group mb-3">
                                <input type="text"
                                    class="form-control @error('npsn-baru')
                                is-invalid
                            @enderror"
                                    placeholder="NPSN" name="npsn-baru" id="npsn-input" value="{{ old('npsn-baru') }}">
                                @error('npsn-baru')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group mb-3">
                                <input type="text"
                                    class="form-control @error('nama-sekolah-baru')
                                is-invalid
                            @enderror"
                                    placeholder="Nama Sekolah" name="nama-sekolah-baru" id="nama-sekolah-input" value="{{ old('nama-sekolah-baru') }}">
                                @error('nama-sekolah-baru')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Ubah</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="modal fade" id="modal-sm">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Hapus Sekolah</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="modal-hapus-sekolah-body">

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <a class="btn btn-primary" id="hapus-sekolah-btn">Hapus</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    @endsection

    @section('script')
        <!-- DataTables  & Plugins -->
        <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
        <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
        <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
        <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

        <script>
            $(function() {
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "responsive": true,
                });

            });
            @if ($errors->hasAny(['npsn', 'nama-sekolah', 'username', 'password']))
                $('#modal-lg').modal('show');
            @endif

            @if ($errors->hasAny(['npsn-baru', 'nama-sekolah-baru']))
                $('#modal-update').modal('show');
            @endif

            function hapus(namaSekolah, npsn) {
                $('#modal-hapus-sekolah-body').html(`Anda yakin ingin menghapus sekolah "${namaSekolah} (${npsn})"`)
                $('#hapus-sekolah-btn').attr('href', `/sekolah/${npsn}/delete`)
                $('#modal-sm').modal('show');
            }

            function ubah(npsn, namaSekolah) {
                $('#npsn-lama-input').val(npsn)
                $('#npsn-input').val(npsn)
                $('#nama-sekolah-input').val(namaSekolah)
                $('#modal-update').modal('show')
            }
        </script>
    @endsection
