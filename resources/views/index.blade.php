@extends('layouts.template')

@section('title', 'Dashboard')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            {{-- @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro', 'Sekolah'])) --}}
            <div class="row">
                @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro', 'Sekolah']))
                    <div class="col-lg-4 col-6">
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{ $siswa }}</h3>
                                <p>Siswa</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-6">
                        <div class="small-box bg-primary">
                            <div class="inner">
                                <h3>{{ $pengajuan }}</h3>
                                <p>Pengajuan</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-6">
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>{{ $pengajuanDiterima }}</h3>
                                <p>Pengajuan Diterima</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-6">
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3>{{ $pengajuanDitolak }}</h3>
                                <p>Pengajuan Ditolak</p>
                            </div>
                        </div>
                    </div>
                @endif
                @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                    <div class="col-lg-4 col-6">
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>{{ $sekolah }}</h3>
                                <p>Sekolah</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-6">
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{ $biro }}</h3>
                                <p>Biro</p>
                            </div>
                        </div>
                    </div>
                @endif
                @if (in_array(Auth::user()->role, ['Sekolah', 'Siswa']))
                    <div class="col-lg-4 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>{{ $jurnalHarian }}</h3>

                                <p>Jurnal Harian</p>
                            </div>
                            {{-- <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div> --}}
                        </div>
                    </div>
                @endif
                @if (in_array(Auth::user()->role, ['Sekolah']))
                    <div class="col-lg-4 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{ $laporanAkhir }}</h3>

                                <p>Laporan Akhir</p>
                            </div>
                            {{-- <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div> --}}
                        </div>
                    </div>
                @endif
            </div>
            {{-- @endif --}}
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Dashboard</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    Selamat Datang di Aplikasi Magang UNG
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    Footer
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
    </div>
@endsection
