@extends('layouts.template')

@section('title', 'Distribusi Siswa')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Magang</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Distribusi Siswa</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <!-- we are adding the accordion ID so Bootstrap's collapse plugin detects it -->
                                <div id="accordion">
                                    @if ($biro->isEmpty())
                                        <div class="text-center">Data Biro Kosong</div>
                                    @else
                                        @foreach ($biro as $b)
                                            <div class="card card-primary">
                                                <div class="card-header">
                                                    <h4 class="card-title w-100">
                                                        <a class="d-block w-100" data-toggle="collapse"
                                                            href="#collapse{{ $b->id }}">
                                                            {{ $b->nama_biro }}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse{{ $b->id }}" class="collapse"
                                                    data-parent="#accordion">

                                                    <div class="card-body">
                                                        <table id="example{{ $b->id }}"
                                                            class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>NISN</th>
                                                                    <th>NIK</th>
                                                                    <th>Nama Lengkap</th>
                                                                    <th>Tempat/Tanggal Lahir</th>
                                                                    <th>Alamat</th>
                                                                    <th>Jenis Kelamin</th>
                                                                    <th>Asal Sekolah</th>
                                                                    <th><button type="button" class="btn btn-default"
                                                                            onclick="showModal('{{ $b->id }}')">
                                                                            Tambah
                                                                        </button></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($b->siswaMagang as $sm)
                                                                    <tr>
                                                                        <td>{{ $sm->siswa->nisn }}</td>
                                                                        <td>{{ $sm->siswa->biodata->nik }}</td>
                                                                        <td>{{ $sm->siswa->biodata->nama_lengkap }}</td>
                                                                        <td>{{ $sm->siswa->biodata->tempat_lahir . ', ' . $sm->siswa->biodata->tanggal_lahir }}
                                                                        </td>
                                                                        <td>{{ $sm->siswa->biodata->alamat }}</td>
                                                                        <td>{{ $sm->siswa->biodata->jenis_kelamin }}</td>
                                                                        <td>{{ $sm->periodeMagang->sekolah->nama_sekolah }}
                                                                        </td>
                                                                        <td><button type="button" class="btn btn-danger"
                                                                                onclick="hapusDistribusi('{{ $sm->siswa->biodata->nama_lengkap }}', '{{ $sm->siswa->nisn }}')">
                                                                                Hapus
                                                                            </button></td>
                                                                    </tr>
                                                                @endforeach

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif


                                    {{-- <div class="card card-danger">
                                        <div class="card-header">
                                            <h4 class="card-title w-100">
                                                <a class="d-block w-100" data-toggle="collapse" href="#collapseTwo">
                                                    Collapsible Group Danger
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                                richardson ad squid.
                                                3
                                                wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck
                                                quinoa nesciunt
                                                laborum
                                                eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                                                single-origin coffee
                                                nulla
                                                assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                                                wes anderson cred
                                                nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                                Leggings occaecat craft
                                                beer
                                                farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard
                                                of them accusamus
                                                labore sustainable VHS.
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('distribusiSiswaPost') }}" method="post">
                    @csrf
                    <input type="hidden" name="id-biro" id="id-biro">
                    <div class="modal-body">
                        <div class="form-group">
                            <select class="select2" multiple="multiple" data-placeholder="Pilih siswa" style="width: 100%;"
                                name="nisn[]">
                                @foreach ($siswa as $s)
                                    <option value="{{ $s->siswa->nisn }}">
                                        {{ $s->siswa->biodata->nama_lengkap . ' (' . $s->siswa->biodata->nik . ')' }}
                                    </option>
                                @endforeach
                            </select>
                            @error('nisn')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal-sm">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Hapus</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-hapus-body">

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <a class="btn btn-primary" id="hapus-btn">Hapus</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('script')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $(function() {
            @foreach ($biro as $b)
                $('#example{{ $b->id }}').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "responsive": true,
                })
            @endforeach
            $('.select2').select2();
        });

        function showModal(id) {
            // console.log(id);
            $('#id-biro').val(id)
            $('#modal-lg').modal('show');
        }

        function hapusDistribusi(namaLengkap, nisn) {
            $('#modal-hapus-body').html(`Anda yakin ingin menghapus distribusi siswa "${namaLengkap} (${nisn})"`)
            $('#hapus-btn').attr('href', `/magang/siswa/distribusi/${nisn}/delete`)
            $('#modal-sm').modal('show');
        }
    </script>
@endsection
