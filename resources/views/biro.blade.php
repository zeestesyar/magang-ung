@extends('layouts.template')

@section('title', 'Biro')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Biro</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Biro</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <a href="{{ route('biroExportPdf') }}" class="btn btn-primary">
                                    PDF
                                </a>
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Nama Biro</th>
                                            <th><button type="button" class="btn btn-default" data-toggle="modal"
                                                    data-target="#modal-lg">
                                                    Tambah
                                                </button></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($biro as $b)
                                            <tr>
                                                <td>{{ $b->nama_biro }}</td>
                                                <td><button type="button" class="btn btn-danger"
                                                        onclick="hapusBiro('{{ $b->nama_biro }}', '{{ $b->id }}')"
                                                        @if ($b->siswa_magang_exists) disabled @endif>
                                                        Hapus
                                                    </button>
                                                    <button type="button" class="btn btn-primary" onclick="ubah('{{ $b->id }}', '{{ $b->nama_biro }}')">
                                                        Ubah
                                                    </button>
                                                    <a href="{{ route('penanggungJawabBiro', $b->id) }}" class="btn btn-primary">Penanggung Jawab</a>
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('biroPost') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="input-group mb-3">
                            <input type="text"
                                class="form-control @error('nama-biro')
                            is-invalid
                        @enderror"
                                placeholder="Nama Biro" name="nama-biro">
                            @error('nama-biro')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-update">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ubah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('biroPut') }}" method="post">
                    @csrf
                    <input type="hidden" name="id-biro" id="id-biro-input">
                    <div class="modal-body">
                        <div class="input-group mb-3">
                            <input type="text"
                                class="form-control @error('nama-biro-baru')
                            is-invalid
                        @enderror"
                                placeholder="Nama Biro" name="nama-biro-baru" id="nama-biro-input">
                            @error('nama-biro-baru')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <div class="modal fade" id="modal-sm">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Hapus Sekolah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-hapus-body">

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <a class="btn btn-primary" id="hapus-btn">Hapus</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection

@section('script')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

    <script>
        $(function() {
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });

        });
        @if ($errors->hasAny(['nama-biro']))
            $('#modal-lg').modal('show');
        @endif

        @if ($errors->hasAny(['nama-biro-baru']))
            $('#modal-update').modal('show');
        @endif

        function hapusBiro(namaBiro, id) {
            $('#modal-hapus-body').html(`Anda yakin ingin menghapus biro "${namaBiro}"`)
            $('#hapus-btn').attr('href', `/biro/${id}/delete`)
            $('#modal-sm').modal('show');
        }

        function ubah(id, namaBiro) {
            $('#id-biro-input').val(id)
            $('#nama-biro-input').val(namaBiro)
            $('#modal-update').modal('show')
        }
    </script>
@endsection
