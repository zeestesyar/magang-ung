@extends('layouts.template')

@section('title', 'Jurnal Kegiatan')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Magang</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Jurnal Kegiatan</h3>
                            </div>
                            <!-- /.card-header -->

                            <div class="card-body">
                                @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro', 'Sekolah']))
                                    <form action="{{ route('jurnalKegiatan') }}" method="get">
                                        @if (Auth::user()->role == 'Sekolah')
                                            <div class="form-group">
                                                <label>Periode Magang</label>
                                                <select class="form-control select2" style="width: 100%;"
                                                    id="id-periode-select">
                                                    <option id="id-periode-option">Pilih Periode</option>
                                                    @foreach ($periodeMagang as $pm)
                                                        <option value="{{ $pm->id }}">
                                                            {{ $pm->tmt . ' - ' . $pm->tst }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Siswa</label>
                                                <select class="form-control select2" style="width: 100%;"
                                                    id="siswa-select"></select>
                                            </div>
                                        @else
                                            <div class="form-group">
                                                <label>Sekolah</label>
                                                <select class="form-control select2" style="width: 100%;"
                                                    id="sekolah-select">
                                                    <option id="sekolah-option">Pilih Sekolah</option>
                                                    @foreach ($sekolah as $s)
                                                        <option value="{{ $s->npsn }}">
                                                            {{ $s->nama_sekolah }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Periode Magang</label>
                                                <select class="form-control select2" style="width: 100%;"
                                                    id="id-periode-select">
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Siswa</label>
                                                <select class="form-control select2" style="width: 100%;"
                                                    id="siswa-select"></select>
                                            </div>
                                        @endif
                                        <button type="submit" class="btn btn-primary mb-3">Cari</button>
                                        @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                                            <a href="{{ route('jurnalExportPdf', request()->query()) }}"
                                                class="btn btn-primary mb-3 mr-1">
                                                PDF
                                            </a>
                                        @endif
                                    </form>
                                @endif
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                                                <th>Asal Sekolah</th>
                                            @endif
                                            @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro', 'Sekolah']))
                                                <th>Nama Siswa</th>
                                                <th>Waktu Magang</th>
                                            @endif
                                            <th>Waktu</th>
                                            <th>Foto</th>
                                            <th>Deskripsi</th>
                                            @if (in_array(Auth::user()->role, ['Siswa']))
                                                <th><button type="button" class="btn btn-default" data-toggle="modal"
                                                        data-target="#modal-lg" @disabled(!$hadir)>
                                                        Tambah
                                                    </button></th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody id="table-body">
                                        @if (Auth::user()->role == 'Siswa')
                                            @foreach ($kehadiranSiswa as $kh)
                                                @foreach ($kh->kegiatanSiswa as $ks)
                                                    <tr>
                                                        <td>{{ $ks->created_at }}</td>
                                                        @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                                                            <td>{{ $ks->siswa->siswaMagang->periodeMagang->sekolah->nama_sekolah }}
                                                            </td>
                                                        @endif

                                                        <td><a class="btn btn-primary"
                                                                href="{{ asset($ks->url_foto) }}">Lihat
                                                                Foto
                                                            </a>
                                                        </td>
                                                        <td>{{ str_replace(["\\r", "\\n"], ["\r", "\n"], $ks->deskripsi) }}
                                                        </td>

                                                        <td><button type="button" class="btn btn-danger"
                                                                onclick="hapusJurnal('{{ $ks->id }}')">
                                                                Hapus
                                                            </button></td>

                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        @elseif(Auth::user()->role == 'Sekolah')
                                            @foreach ($table as $pm)
                                                @foreach ($pm->siswaMagang as $sm)
                                                    @foreach ($sm->kehadiranSiswa as $kh)
                                                        @foreach ($kh->kegiatanSiswa as $ks)
                                                            <tr>
                                                                <td>{{ $sm->siswa->biodata->nama_lengkap }}</td>
                                                                <td>{{ $pm->tmt . ' - ' . $pm->tst }}</td>
                                                                <td>{{ $ks->created_at }}</td>
                                                                <td><a class="btn btn-primary"
                                                                        href="{{ asset($ks->url_foto) }}">Lihat
                                                                        Foto
                                                                    </a>
                                                                </td>
                                                                <td>{{ str_replace(["\\r", "\\n"], ["\r", "\n"], $ks->deskripsi) }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        @else
                                            @foreach ($table as $s)
                                                @foreach ($s->periodeMagang as $pm)
                                                    @foreach ($pm->siswaMagang as $sm)
                                                        @foreach ($sm->kehadiranSiswa as $kh)
                                                            @foreach ($kh->kegiatanSiswa as $ks)
                                                                <tr>
                                                                    <td>{{ $s->nama_sekolah }}</td>
                                                                    <td>{{ $sm->siswa->biodata->nama_lengkap }}</td>
                                                                    <td>{{ $pm->tmt . ' - ' . $pm->tst }}</td>
                                                                    <td>{{ $ks->created_at }}</td>
                                                                    <td><a class="btn btn-primary"
                                                                            href="{{ asset($ks->url_foto) }}">Lihat
                                                                            Foto
                                                                        </a>
                                                                    </td>
                                                                    <td>{{ str_replace(["\\r", "\\n"], ["\r", "\n"], $ks->deskripsi) }}
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    @if (in_array(Auth::user()->role, ['Siswa']))
        @if ($hadir)
            <div class="modal fade" id="modal-lg">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Tambah</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{ route('jurnalKegiatanPost') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id-kehadiran" value="{{ $kehadiranSiswa->first()->id }}">
                            <div class="modal-body">
                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="file"
                                            accept="image/jpeg">
                                        <label class="custom-file-label" for="exampleInputFile">Pilih file</label>
                                    </div>
                                    @error('file')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <textarea class="form-control" rows="3" placeholder="Masukkan deskripsi..." name="deskripsi"></textarea>
                                    @error('deskripsi')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        @endif

        <div class="modal fade" id="modal-sm">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Hapus Jurnal</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="modal-hapus-body">

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <a class="btn btn-primary" id="hapus-btn">Hapus</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    @endif
@endsection

@section('script')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>

    <script>
        $(function() {


            // $('.select2').select2()
            $('.select2').select2({
                theme: 'bootstrap4'
            })

            const table = $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });



            @if (Auth::user()->role == 'Sekolah')
                const periodeMagang = {!! $periodeMagang !!};
                $('#id-periode-select').on('change', function() {
                    let selected = periodeMagang.find(function(element) {
                        return element.id === $('#id-periode-select').val()
                    })
                    $(this).attr('name', 'id-periode')
                    $('#id-periode-option').attr('disabled', true)
                    $('#siswa-select').html('');
                    $('#siswa-select').append(
                        `<option id="siswa-option">Pilih Siswa</option>`
                    )
                    selected.siswa_magang.forEach(sm => {
                        $('#siswa-select').append(
                            `<option value="${sm.nisn}">${sm.siswa.biodata.nama_lengkap} (${sm.nisn})</option>`
                        )
                    });
                })

                $('#siswa-select').on('change', function() {
                    $(this).attr('name', 'siswa')
                    $('#siswa-option').attr('disabled', true)
                })
            @elseif (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                const sekolah = {!! $sekolah !!};

                $('#sekolah-select').on('change', function() {
                    $(this).attr('name', 'sekolah')
                    $('#sekolah-option').attr('disabled', true)
                    $('#id-periode-select').html('');
                    $('#id-periode-select').append(
                        `<option id="id-periode-option">Pilih Periode</option>`
                    )
                    sekolah.find(function(element) {
                        return element.npsn === $('#sekolah-select').val()
                    }).periode_magang.forEach(pm => {
                        $('#id-periode-select').append(
                            `<option value="${pm.id}">${pm.tmt} - ${pm.tst}</option>`
                        )
                    })
                })

                $('#id-periode-select').on('change', function() {
                    $(this).attr('name', 'id-periode')
                    $('#id-periode-option').attr('disabled', true)
                    $('#siswa-select').html('');
                    $('#siswa-select').append(
                        `<option id="siswa-option">Pilih Siswa</option>`
                    )
                    sekolah.find(element => {
                            return element.npsn === $('#sekolah-select').val()
                        }).periode_magang.find(pm => {
                            return pm.id === $('#id-periode-select').val()
                        })
                        .siswa_magang.forEach(sm => {
                            $('#siswa-select').append(
                                `<option value="${sm.nisn}">${sm.siswa.biodata.nama_lengkap} (${sm.nisn})</option>`
                            )
                        });
                })

                $('#siswa-select').on('change', function() {
                    $(this).attr('name', 'siswa')
                    $('#siswa-option').attr('disabled', true)
                })
            @endif

        });

        @if (in_array(Auth::user()->role, ['Siswa']))
            @if ($hadir)
                @if ($errors->any())
                    $('#modal-lg').modal('show');
                @endif
            @endif

            function hapusJurnal(id) {
                $('#modal-hapus-body').html(`Anda yakin ingin menghapus jurnal kegiatan`)
                $('#hapus-btn').attr('href', `/magang/jurnal-kegiatan/${id}/delete`)
                $('#modal-sm').modal('show');
            }
        @endif
    </script>
@endsection
