@use(Illuminate\Support\Carbon)

@extends('layouts.template')

@section('title', 'Absensi')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Magang</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Batas Absensi</h3>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('batasAbsenPut') }}" method="post">
                                        @csrf
                                        <label>Batas Absensi</label>

                                        <div class="input-group date mb-3" id="timepicker" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input"
                                                data-target="#timepicker" name="batas-absensi"
                                                value="{{ $batasAbsen }}" />
                                            <div class="input-group-append" data-target="#timepicker"
                                                data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="far fa-clock"></i></div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary mb-3">Ubah</button>
                                    </form>
                                </div>
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Absensi</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro', 'Sekolah']))
                                    <form action="{{ route('absensi') }}" method="get">
                                        @if (Auth::user()->role == 'Sekolah')
                                            <div class="form-group">
                                                <label>Periode Magang</label>
                                                <select class="form-control select2" style="width: 100%;"
                                                    id="id-periode-select">
                                                    <option id="id-periode-option">Pilih Periode</option>
                                                    @foreach ($periodeMagang as $pm)
                                                        <option value="{{ $pm->id }}">
                                                            {{ $pm->tmt . ' - ' . $pm->tst }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Siswa</label>
                                                <select class="form-control select2" style="width: 100%;"
                                                    id="siswa-select"></select>
                                            </div>
                                        @else
                                            <div class="form-group">
                                                <label>Sekolah</label>
                                                <select class="form-control select2" style="width: 100%;"
                                                    id="sekolah-select">
                                                    <option id="sekolah-option">Pilih Sekolah</option>
                                                    @foreach ($sekolah as $s)
                                                        <option value="{{ $s->npsn }}">
                                                            {{ $s->nama_sekolah }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Periode Magang</label>
                                                <select class="form-control select2" style="width: 100%;"
                                                    id="id-periode-select">
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Siswa</label>
                                                <select class="form-control select2" style="width: 100%;"
                                                    id="siswa-select"></select>
                                            </div>
                                        @endif
                                        <button type="submit" class="btn btn-primary mb-3">Cari</button>
                                        @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                                            <a href="{{ route('absensiExportPdf', request()->query()) }}"
                                                class="btn btn-primary mb-3 mr-1">
                                                PDF
                                            </a>
                                        @endif
                                    </form>
                                @endif
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                                                <th>Sekolah</th>
                                            @endif
                                            @if (in_array(Auth::user()->role, ['Admin', 'Petugas Biro', 'Sekolah']))
                                                <th>Periode Magang</th>
                                                <th>Siswa</th>
                                            @endif
                                            <th>Tanggal</th>
                                            <th>Status</th>
                                            <th>Foto</th>
                                            @if (Auth::user()->role == 'Siswa')
                                                <th><button type="button" class="btn btn-default" data-toggle="modal"
                                                        data-target="#modal-lg"
                                                        @if (!$bisaAbsen) disabled @endif>
                                                        Absen
                                                    </button></th>
                                            @endif

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (Auth::user()->role == 'Siswa')
                                            @foreach ($kehadiranSiswa as $ks)
                                                <tr>
                                                    <td>{{ $ks->tanggal }}</td>
                                                    <td>
                                                        @if ($ks->sts_hadir == 'Hadir')
                                                            <span class="badge bg-primary">{{ $ks->sts_hadir }}</span>
                                                        @else
                                                            <button type="button" class="btn btn-warning"
                                                                onclick="lihatAlasan('{{ $ks->alasan }}', '{{ $ks->sts_hadir }}')"><b>{{ $ks->sts_hadir }}</b>
                                                                (Lihat Alasan)
                                                                </a>
                                                        @endif

                                                    </td>
                                                    <td><a class="btn btn-primary" href="{{ asset($ks->url_foto) }}">Lihat
                                                            Foto
                                                        </a></td>
                                                    <td><button type="button" class="btn btn-danger"
                                                            onclick="batal('{{ $ks->id }}', '{{ $ks->tanggal }}')"
                                                            @disabled(Carbon::today()->gt(Carbon::parse($ks->tanggal)) || $ks->kegiatan_siswa_exists)>
                                                            Batalkan
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @elseif(Auth::user()->role == 'Sekolah')
                                            @foreach ($table as $pm)
                                                @foreach ($pm->siswaMagang as $sm)
                                                    @foreach ($sm->kehadiranSiswa as $ks)
                                                        <tr>
                                                            <td>{{ $pm->tmt . ' - ' . $pm->tst }}</td>
                                                            <td>{{ $sm->siswa->biodata->nama_lengkap . ' (' . $sm->siswa->nisn . ')' }}
                                                            </td>
                                                            <td>{{ $ks->tanggal }}</td>
                                                            <td>
                                                                @if ($ks->sts_hadir == 'Hadir')
                                                                    <span
                                                                        class="badge bg-primary">{{ $ks->sts_hadir }}</span>
                                                                @else
                                                                    <button type="button" class="btn btn-warning"
                                                                        onclick="lihatAlasan('{{ $ks->alasan }}', '{{ $ks->sts_hadir }}')"><b>{{ $ks->sts_hadir }}</b>
                                                                        (Lihat Alasan)
                                                                    </button>
                                                                @endif

                                                            </td>
                                                            <td><a class="btn btn-primary"
                                                                    href="{{ asset($ks->url_foto) }}">Lihat
                                                                    Foto
                                                                </a></td>
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        @else
                                            @foreach ($table as $s)
                                                @foreach ($s->periodeMagang as $pm)
                                                    @foreach ($pm->siswaMagang as $sm)
                                                        @foreach ($sm->kehadiranSiswa as $ks)
                                                            <tr>

                                                                <td>{{ $s->nama_sekolah . ' (' . $s->npsn . ')' }}
                                                                </td>
                                                                <td>{{ $pm->tmt . ' - ' . $pm->tst }}</td>
                                                                <td>{{ $sm->siswa->biodata->nama_lengkap . ' (' . $sm->siswa->nisn . ')' }}
                                                                </td>
                                                                <td>{{ $ks->tanggal }}</td>
                                                                <td>
                                                                    @if ($ks->sts_hadir == 'Hadir')
                                                                        <span
                                                                            class="badge bg-primary">{{ $ks->sts_hadir }}</span>
                                                                    @else
                                                                        <button type="button" class="btn btn-warning"
                                                                            onclick="lihatAlasan('{{ $ks->alasan }}', '{{ $ks->sts_hadir }}')"><b>{{ $ks->sts_hadir }}</b>
                                                                            (Lihat Alasan)
                                                                        </button>
                                                                    @endif

                                                                </td>
                                                                <td><a class="btn btn-primary"
                                                                        href="{{ asset($ks->url_foto) }}">Lihat
                                                                        Foto
                                                                    </a></td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="modal-foto">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal-alasan-title">Foto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="" id="img-foto">
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal-alasan">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal-alasan-title">Alasan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <textarea class="form-control" rows="3" id="alasan-textarea" readonly></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    @if (Auth::user()->role == 'Siswa')
        <div class="modal fade" id="modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Absen</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('absensiPost') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="exampleInputFile" name="file"
                                        accept="image/jpeg">
                                    <label class="custom-file-label" for="exampleInputFile">Pilih file</label>
                                </div>
                                @error('file')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group clearfix">
                                <div class="icheck-primary d-inline mr-2">
                                    <input type="radio" id="hadir-radio" name="sts-hadir" value="Hadir"
                                        @if (old('sts-hadir') == 'Hadir') checked @endif>
                                    <label for="hadir-radio">
                                        Hadir
                                    </label>
                                </div>
                                <div class="icheck-primary d-inline mr-2">
                                    <input type="radio" id="izin-radio" name="sts-hadir" value="Izin"
                                        @if (old('sts-hadir') == 'Izin') checked @endif>
                                    <label for="izin-radio">
                                        Izin
                                    </label>
                                </div>
                                <div class="icheck-primary d-inline">
                                    <input type="radio" id="sakit-radio" name="sts-hadir" value="Sakit"
                                        @if (old('sts-hadir') == 'Sakit') checked @endif>
                                    <label for="sakit-radio">
                                        Sakit
                                    </label>
                                </div>
                                @error('sts-hadir')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror


                            </div>
                            <div class="form-group d-none" id="alasan-form">
                                <textarea class="form-control" rows="3" placeholder="Masukkan alasan..." name="alasan">{{ old('alasan') }}</textarea>
                                @error('alasan')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Absen</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="modal fade" id="modal-sm">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Batalkan Absen</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="modal-hapus-body">

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <a class="btn btn-primary" id="hapus-btn">Batalkan</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    @endif
@endsection

@section('script')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        $(function() {
            $('.select2').select2({
                theme: 'bootstrap4'
            })
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });

            $('#timepicker').datetimepicker({
                format: 'HH:mm:ss'
            })

            @if (Auth::user()->role == 'Sekolah')
                const periodeMagang = {!! $periodeMagang !!};
                $('#id-periode-select').on('change', function() {
                    let selected = periodeMagang.find(function(element) {
                        return element.id === $('#id-periode-select').val()
                    })
                    $(this).attr('name', 'id-periode')
                    $('#id-periode-option').attr('disabled', true)
                    $('#siswa-select').html('');
                    $('#siswa-select').append(
                        `<option id="siswa-option">Pilih Siswa</option>`
                    )
                    selected.siswa_magang.forEach(sm => {
                        $('#siswa-select').append(
                            `<option value="${sm.nisn}">${sm.siswa.biodata.nama_lengkap} (${sm.nisn})</option>`
                        )
                    });
                })

                $('#siswa-select').on('change', function() {
                    $(this).attr('name', 'siswa')
                    $('#siswa-option').attr('disabled', true)
                })
            @elseif (in_array(Auth::user()->role, ['Admin', 'Petugas Biro']))
                const sekolah = {!! $sekolah !!};

                $('#sekolah-select').on('change', function() {
                    $(this).attr('name', 'sekolah')
                    $('#sekolah-option').attr('disabled', true)
                    $('#id-periode-select').html('');
                    $('#id-periode-select').append(
                        `<option id="id-periode-option">Pilih Periode</option>`
                    )
                    sekolah.find(function(element) {
                        return element.npsn === $('#sekolah-select').val()
                    }).periode_magang.forEach(pm => {
                        $('#id-periode-select').append(
                            `<option value="${pm.id}">${pm.tmt} - ${pm.tst}</option>`
                        )
                    })
                })

                $('#id-periode-select').on('change', function() {
                    $(this).attr('name', 'id-periode')
                    $('#id-periode-option').attr('disabled', true)
                    $('#siswa-select').html('');
                    $('#siswa-select').append(
                        `<option id="siswa-option">Pilih Siswa</option>`
                    )
                    sekolah.find(element => {
                            return element.npsn === $('#sekolah-select').val()
                        }).periode_magang.find(pm => {
                            return pm.id === $('#id-periode-select').val()
                        })
                        .siswa_magang.forEach(sm => {
                            $('#siswa-select').append(
                                `<option value="${sm.nisn}">${sm.siswa.biodata.nama_lengkap} (${sm.nisn})</option>`
                            )
                        });
                })

                $('#siswa-select').on('change', function() {
                    $(this).attr('name', 'siswa')
                    $('#siswa-option').attr('disabled', true)
                })
            @endif
        });

        @if (Auth::user()->role == 'Siswa')
            @if ($errors->any())
                @if (in_array(old('sts-hadir'), ['Izin', 'Sakit']))
                    $('#alasan-form').removeClass('d-none')
                @endif
                $('#modal-lg').modal('show');
            @endif

            $('[name="sts-hadir"]').on('change', function() {
                switch ($(this).val()) {
                    case 'Izin':
                        $('#alasan-form').removeClass('d-none')
                        break
                    case 'Sakit':
                        $('#alasan-form').removeClass('d-none')
                        break
                    default:
                        $('#alasan-form').addClass('d-none')
                        break;
                }
            })

            function batal(id, tanggal) {
                $('#modal-hapus-body').html(`Anda yakin ingin mengbatalkan absen tanggal ${tanggal}`)
                $('#hapus-btn').attr('href', `/magang/absensi/${id}/batal`)
                $('#modal-sm').modal('show');
            }
        @endif

        function lihatAlasan(alasan, stsHadir) {
            $('#alasan-textarea').val(alasan)
            $('#modal-alasan-title').html(`Alasan ${stsHadir}`)
            $('#modal-alasan').modal('show')
        }

        // function lihatFoto(url) {
        //     $('#img-foto').attr('src', url)
        //     $('#modal-foto').modal('show')
        // }
    </script>
@endsection
