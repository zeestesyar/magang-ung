@extends('layouts.template')

@section('title', 'Siswa Magang')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Biro</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Siswa Magang</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form action="{{ route('siswaMagang') }}" method="get">
                                    <div class="form-group">
                                        <label>Biro</label>
                                        <select class="form-control select2" style="width: 100%;" id="biro-select">
                                            <option id="biro-option">Pilih Biro</option>
                                            @foreach ($biro as $b)
                                                <option value="{{ $b->id }}">
                                                    {{ $b->nama_biro }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Periode Magang</label>
                                        <select class="form-control select2" style="width: 100%;" id="id-periode-select">
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary mb-3">Cari</button>
                                    <a href="{{ route('siswaMagangExportPdf', request()->query()) }}"
                                        class="btn btn-primary mb-3">
                                        PDF
                                    </a>
                                </form>
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Biro</th>
                                            <th>Periode Magang</th>
                                            <th>Sekolah</th>
                                            <th>Siswa</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($table as $b)
                                            @foreach ($b->siswaMagang as $sm)
                                                @if (!is_null($sm->periodeMagang))
                                                    <tr>
                                                        <td>{{ $b->nama_biro }}</td>
                                                        <td>{{ $sm->periodeMagang->tmt . ' - ' . $sm->periodeMagang->tst }}
                                                        </td>
                                                        <td>{{ $sm->periodeMagang->sekolah->nama_sekolah }}</td>
                                                        <td>{{ $sm->siswa->biodata->nama_lengkap }}</td>
                                                        {{-- <td>{{ is_null($sm->periodeMagang) }}</td>
                                                <td>{{ is_null($sm->periodeMagang) }}</td>
                                                <td>{{ is_null($sm->siswa->biodata->nama_lengkap) }}</td> --}}
                                                        <td>
                                                            @if (is_null($sm->penilaian))
                                                                <button type="button" class="btn btn-default"
                                                                    onclick="penilaian('{{ $sm->nisn }}')">
                                                                    Penilaian
                                                                </button>
                                                            @else
                                                                {{ $sm->penilaian }}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Penilaian</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('penilaianSiswaMagang') }}" method="post">
                    @csrf
                    <input type="hidden" name="nisn" id="nisn-input" value="{{ old('nisn') }}">
                    <div class="modal-body">
                        <div class="input-group mb-3">
                            <input type="text"
                                class="form-control @error('nilai')
                            is-invalid
                        @enderror"
                                placeholder="Nilai" name="nilai" value="{{ old('nilai') }}">
                            @error('nilai')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection

@section('script')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $(function() {
            $('.select2').select2({
                theme: 'bootstrap4'
            })
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
            @if ($errors->any())
                $('#modal-lg').modal('show');
            @endif

            $('#biro-select').on('change', function() {
                const biro = {!! $biro !!};
                $(this).attr('name', 'biro')
                $('#biro-option').attr('disabled', true)
                $('#id-periode-select').html('');
                $('#id-periode-select').append(
                    `<option id="id-periode-option">Pilih Periode</option>`
                )
                biro.find(function(element) {
                    return element.id === $('#biro-select').val()
                }).siswa_magang.forEach(element => {
                    $('#id-periode-select').append(
                        `<option value="${element.periode_magang.id}">${element.periode_magang.tmt} - ${element.periode_magang.tst}</option>`
                    )
                });
            })

            $('#id-periode-select').on('change', function() {
                $(this).attr('name', 'id-periode')
                $('#id-periode-option').attr('disabled', true)
            })


        });

        function penilaian(id) {
            $('#nisn-input').val(id)
            $('#modal-lg').modal('show');
        }
    </script>
@endsection
