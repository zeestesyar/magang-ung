<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('siswa_magang', function (Blueprint $table) {
            // $table->primary(['nisn', 'id_periode']);
            $table->string('nisn', 10)->primary();
            $table->uuid('id_periode');
            $table->uuid('id_biro')->nullable();
            $table->integer('penilaian')->nullable();
            $table->string('url_laporan', 255)->nullable();
            $table->timestamps();

            $table->foreign('nisn')->references('nisn')->on('siswa')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_periode')->references('id')->on('periode_magang')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_biro')->references('id')->on('biro')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('siswa_magang');
    }
};
