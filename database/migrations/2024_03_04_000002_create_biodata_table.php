<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('biodata', function (Blueprint $table) {
            $table->string('nik', 16)->primary();
            $table->string('nama_lengkap', 50);
            $table->date('tanggal_lahir');
            $table->string('tempat_lahir', 50);
            $table->string('alamat', 255);
            $table->enum('jenis_kelamin', ['Laki - laki', 'Perempuan']);
            $table->uuid('id_user')->unique()->nullable();
            $table->string('no_telp')->unique()->nullable();
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('user')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('biodata');
    }
};
