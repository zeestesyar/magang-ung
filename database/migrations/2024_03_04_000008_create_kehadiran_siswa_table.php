<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kehadiran_siswa', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('nisn');
            $table->date('tanggal');
            $table->enum('sts_hadir', ['Hadir', 'Izin', 'Sakit']);
            $table->string('url_foto', 255);
            $table->text('alasan')->nullable();
            $table->timestamps();

            $table->unique(['nisn', 'tanggal']);

            $table->foreign('nisn')->references('nisn')->on('siswa_magang')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kehadiran_siswa');
    }
};
