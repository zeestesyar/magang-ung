<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kegiatan_siswa', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('url_foto', 255);
            $table->text('deskripsi');
            $table->uuid('id_kehadiran');
            $table->timestamps();

            $table->foreign('id_kehadiran')->references('id')->on('kehadiran_siswa')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kegiatan_siswa');
    }
};
