<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('periode_magang', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->date('tmt');
            $table->date('tst');
            $table->string('npsn', 8);
            $table->string('url_surat', 255);
            $table->boolean('sts_persetujuan')->nullable();
            $table->text('alasan')->nullable();
            $table->timestamps();

            $table->foreign('npsn')->references('npsn')->on('sekolah')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('periode_magang');
    }
};
