<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->string('nisn', 10)->primary();
            $table->string('nik', 16)->unique();
            $table->string('npsn', 8)->nullable();
            $table->timestamps();

            $table->foreign('nik')->references('nik')->on('biodata')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('npsn')->references('npsn')->on('sekolah')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('siswa');
    }
};
