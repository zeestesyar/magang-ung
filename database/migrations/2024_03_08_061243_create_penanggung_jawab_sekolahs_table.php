<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('penanggung_jawab_sekolah', function (Blueprint $table) {
            $table->string('nik', 16)->primary();
            $table->string('npsn', 8);
            $table->timestamps();

            $table->foreign('nik')->references('nik')->on('biodata')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('npsn')->references('npsn')->on('sekolah')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('penanggung_jawab_sekolah');
    }
};
