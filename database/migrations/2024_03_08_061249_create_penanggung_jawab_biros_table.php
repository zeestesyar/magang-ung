<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('penanggung_jawab_biro', function (Blueprint $table) {
            $table->string('nik')->primary();
            $table->uuid('id_biro');
            $table->timestamps();

            $table->foreign('nik')->references('nik')->on('biodata')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_biro')->references('id')->on('biro')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('penanggung_jawab_biro');
    }
};
