<?php

namespace Database\Seeders;

use App\Models\Biodata;
use App\Models\User;
use App\Models\Sekolah;
use App\Models\Siswa;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // $userAdmin = new User();
        // $userAdmin->username = 'admin';
        // $userAdmin->password = bcrypt('admin');
        // $userAdmin->role = 'Admin';
        // $userAdmin->save();

        $userBiro = new User();
        $userBiro->username = 'biro';
        $userBiro->password = bcrypt('biro');
        $userBiro->role = 'Petugas Biro';
        $userBiro->save();

        // $userSekolah = new User();
        // $userSekolah->username = 'sekolah';
        // $userSekolah->password = bcrypt('sekolah');
        // $userSekolah->role = 'Sekolah';
        // $userSekolah->save();

        // $userSiswa = new User();
        // $userSiswa->username = 'siswa';
        // $userSiswa->password = bcrypt('siswa');
        // $userSiswa->role = 'Siswa';
        // $userSiswa->save();

        // $biodataAdmin = new Biodata();
        // $biodataAdmin->nik = '0000000000000000';
        // $biodataAdmin->nama_lengkap = 'Admin';
        // $biodataAdmin->tanggal_lahir = '2000-01-01';
        // $biodataAdmin->tempat_lahir = 'Gorontalo';
        // $biodataAdmin->alamat = 'Gorontalo';
        // $biodataAdmin->jenis_kelamin = 'Laki - laki';
        // $biodataAdmin->id_user = $userAdmin->id;
        // $biodataAdmin->save();

        $biodataBiro = new Biodata();
        $biodataBiro->nik = '0000000000000001';
        $biodataBiro->nama_lengkap = 'Biro';
        $biodataBiro->tanggal_lahir = '2000-01-01';
        $biodataBiro->tempat_lahir = 'Gorontalo';
        $biodataBiro->alamat = 'Gorontalo';
        $biodataBiro->jenis_kelamin = 'Perempuan';
        $biodataBiro->id_user = $userBiro->id;
        $biodataBiro->save();

        // $biodataSiswa = new Biodata();
        // $biodataSiswa->nik = '0000000000000002';
        // $biodataSiswa->nama_lengkap = 'Siswa';
        // $biodataSiswa->tanggal_lahir = '2000-01-01';
        // $biodataSiswa->tempat_lahir = 'Gorontalo';
        // $biodataSiswa->alamat = 'Gorontalo';
        // $biodataSiswa->jenis_kelamin = 'Laki - laki';
        // $biodataSiswa->id_user = $userSiswa->id;
        // $biodataSiswa->save();

        // $sekolah = new Sekolah();
        // $sekolah->npsn = '00000000';
        // $sekolah->nama_sekolah = 'Sekolah';
        // $sekolah->id_user = $userSekolah->id;
        // $sekolah->save();

        // $siswa = new Siswa();
        // $siswa->nisn = '0000000000';
        // $siswa->nik = $biodataSiswa->nik;
        // $siswa->npsn = $sekolah->npsn;
        // $siswa->save();
    }
}
